//
//  ransac.cpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/06/13.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#include "ransac.hpp"

bool isShort(LineSegment ls) {
    return (ls.abs() < 300.0);
}

bool isSameOrientation(LineSegment ls1, LineSegment ls2) {
    double deltaAngle = abs(RAD2DEG(ls1.angle()) - RAD2DEG(ls2.angle()));
//    cout << deltaAngle << ", " << RAD2DEG(ls1.angle()) << ", " << RAD2DEG(ls2.angle()) << endl;
    if (deltaAngle < 10.0 || deltaAngle > 170.0) {
        return true;
    } else {
        return false;
    }
}

void sort(vector<LineSegment> &vls) {
    for(int i=0; i<vls.size()-1; ++i) {
        for(int j=i+1;j<vls.size(); ++j) {
            if(vls[i].abs() <= vls[j].abs()) swap(vls[i], vls[j]);
        }
    }
}

vector<vector<Point>> Ransac::grouping(const vector<Point> &points) {
    vector<vector<Point>> groups;
    int groupIdx = -1;
    Point prev = Point(0, 0);
    for (int i=0; i<points.size(); ++i) {
        if ((prev.x==0 && prev.y==0) || (prev.distance(points[i]) >= 800)) {
            //new group
            groups.push_back(vector<Point>({points[i]}));
            groupIdx += 1;
        } else {
            groups[groupIdx].push_back(points[i]);
        }
        prev = points[i];
    }
    groups.erase(remove_if(groups.begin(), groups.end(), [](vector<Point> vp)->bool {
        if (vp.size() < 10) return true;
        return false;
    }), groups.end());
    return groups;
}

vector<LineSegment> ransac(vector<Point> &tmpPoints) {
    vector<LineSegment> lineSegments;
    vector<Point> points(tmpPoints);
    
    
    
    return lineSegments;
}

vector< LineSegment > Ransac::ransacAlgorithm(const vector<Point> &points){
    double threshold = 30.0;
    vector<tuple<Point, bool>> dataPoints;
    vector<LineSegment> vls;
    srand((unsigned)time(NULL));
    
    dataPoints.clear();
    for(Point p: points) {
        dataPoints.push_back(make_tuple(p, false));
    }
    int tooShort = 0;
    
    int randomIndex = 0;
    int baseIndex = 0;
    int inlier = 0;
    int maxInlier = 0;
    Line estimatedLine = Line(0.0, 0.0);
    vector<Point> pointsToDetectLine;
    while (tooShort < 100){
        inlier = 0;
        maxInlier = 0;
        //100ステップで直線を1本検出
        for (int step=0; step<100; ++step) {
            pointsToDetectLine.clear();
            //5点選択
            
            do {
                randomIndex = rand()*(dataPoints.size()+1.0)/(RAND_MAX + 1.0);
            } while (get<1>(*(dataPoints.begin()+randomIndex)) == true);
            pointsToDetectLine.push_back(get<0>(*(dataPoints.begin()+randomIndex)));
            
            for (int i=1; i<3; ++i) {
                pointsToDetectLine.push_back(get<0>(*(dataPoints.begin()+(randomIndex+i)%dataPoints.size())));
                pointsToDetectLine.push_back(get<0>(*(dataPoints.begin()+(randomIndex-i)%dataPoints.size())));
            }
            //直線を推定
            Line detectedLine = estimateLine(pointsToDetectLine);
            //直線上の点の数をカウント
            inlier = 0;
            vector<tuple<Point, bool>>::iterator it = dataPoints.begin();
            while (it != dataPoints.end()) {
                if (detectedLine.distancePoint(get<0>(*it)) < threshold && get<1>(*it)==false) {
                    inlier += 1;
                }
                ++ it;
            }
            //点の数が最大となるようにdetectedLine, randomIndex, inlierを保持
            if (inlier > maxInlier) {
                maxInlier = inlier;
                baseIndex = randomIndex;
                estimatedLine = detectedLine;
            }
        }
        //baseIndexから戻る方と進む方に関して直線上の点をそれぞれカウント
        Point startPoint, endPoint;
        
        int count = 0;
        int startIndex = baseIndex, endIndex = baseIndex;
        int delta = baseIndex + 1;
        startPoint = get<0>(*(dataPoints.begin()+baseIndex));
        endPoint = get<0>(*(dataPoints.begin()+baseIndex));
        while (count == 0) {
            if (estimatedLine.distancePoint(get<0>(*(dataPoints.begin()+delta%dataPoints.size()))) < threshold
                && get<1>(*(dataPoints.begin()+delta%dataPoints.size())) == false) {
                if (get<0>(*(dataPoints.begin()+delta%dataPoints.size())).distance(startPoint) < 500.0) {
                    startPoint = get<0>(*(dataPoints.begin()+delta%dataPoints.size()));
                    startIndex = delta % dataPoints.size();
                    count = 0;
                } else {
                    count += 1;
                }
            } else {
                count += 1;
            }
            delta++;
        }
        
        count = 0;
        int i = 1;
        while (count == 0) {
            int delta = baseIndex - i;
            if (delta < 0) delta += dataPoints.size();
            if (estimatedLine.distancePoint(get<0>(*(dataPoints.begin()+delta%dataPoints.size()))) < threshold
                && get<1>(*(dataPoints.begin()+delta%dataPoints.size())) == false) {
                if (get<0>(*(dataPoints.begin()+delta%dataPoints.size())).distance(endPoint) < 500.0) {
                    endPoint = get<0>(*(dataPoints.begin()+delta%dataPoints.size()));
                    endIndex = delta%dataPoints.size();
                    count = 0;
                } else {
                    count += 1;
                }
            } else {
                count += 1;
            }
            ++i;
        }
        //要検討．
        LineSegment ls = LineSegment(estimatedLine.getIntersectionOfPerpendicular(startPoint), estimatedLine.getIntersectionOfPerpendicular(endPoint));
        if (ls.abs() < 100.0){
            tooShort += 1;
            continue;
        }
        vls.push_back(ls);
        
        for (int count=0;;++count) {
            int i = (endIndex+count)%dataPoints.size();
            if (i==startIndex%dataPoints.size()) break;
            
            get<1>(*(dataPoints.begin()+i)) = true;
        }
        
        tooShort = 0;
    }
    
//    vector<LineSegment> mergedLS = mergeLineSegments(vls, 300.0);
//    return mergedLS;
    
    merge(vls, 300.0);
    return vls;
}

//5点から直線を求める．
Line Ransac::estimateLine(const vector<Point> &points) {
    vector<double> vRho;
    double minRho = INF;
    double rho = 0, theta = 0;
    for (double deg=0; deg<180; deg+=0.25) {
        double rad = DEG2RAD(deg);
        vRho.clear();
        for (Point p: points) {
            vRho.push_back(p.x*cos(rad) + p.y*sin(rad));
        }
        double dRho = abs(*(max_element(vRho.begin(), vRho.end())) - *(min_element(vRho.begin(), vRho.end())));
        if (dRho - minRho < 0) {
            minRho = dRho;
            rho = accumulate(vRho.begin(), vRho.end(), 0.0) / vRho.size();
            theta = rad;
        }
    }
    return Line(rho, theta);
}

vector<LineSegment> Ransac::mergeLineSegments(vector<LineSegment> vls, double threshold) {
    vector<LineSegment> mergedLSinVLS;
    
    mergedLSinVLS.clear();
    for (int i=0; i<vls.size(); ++i) {
        for (int j=i+1; j<vls.size(); ++j) {
            mergedLSinVLS.clear();
            LineSegment ls1 = vls[i], ls2 = vls[j];
            if (ls1.p.distance(ls2.q) < threshold && isSameOrientation(ls1, ls2)) {
                mergedLSinVLS.push_back(ls1);
                mergedLSinVLS.push_back(ls2);
                vls.erase(remove_if(vls.begin(), vls.end(),[&mergedLSinVLS](LineSegment ls)->bool
                                    {
                                        if (find(mergedLSinVLS.begin(), mergedLSinVLS.end(),ls)!=mergedLSinVLS.end()) return true;
                                        else return false;
                                    }), vls.end());
                vls.push_back(LineSegment(ls1.q, ls2.p));
                i=0;j=1;
            }else if (ls1.q.distance(ls2.p) < threshold && isSameOrientation(ls1, ls2)) {
                mergedLSinVLS.push_back(ls1);
                mergedLSinVLS.push_back(ls2);
                vls.erase(remove_if(vls.begin(), vls.end(),[&mergedLSinVLS](LineSegment ls)->bool
                                    {
                                        if (find(mergedLSinVLS.begin(), mergedLSinVLS.end(),ls)!=mergedLSinVLS.end()) return true;
                                        else return false;
                                    }), vls.end());
                vls.push_back(LineSegment(ls1.p, ls2.q));
                i=0;j=1;
            }else if (ls1.p.distance(ls2.p) < threshold && isSameOrientation(ls1, ls2)) {
                mergedLSinVLS.push_back(ls1);
                mergedLSinVLS.push_back(ls2);
                vls.erase(remove_if(vls.begin(), vls.end(),[&mergedLSinVLS](LineSegment ls)->bool
                                    {
                                        if (find(mergedLSinVLS.begin(), mergedLSinVLS.end(),ls)!=mergedLSinVLS.end()) return true;
                                        else return false;
                                    }), vls.end());
                vls.push_back(LineSegment(ls1.q, ls2.q));
                i=0;j=1;
            }else if (ls1.q.distance(ls2.q) < threshold && isSameOrientation(ls1, ls2)) {
                mergedLSinVLS.push_back(ls1);
                mergedLSinVLS.push_back(ls2);
                vls.erase(remove_if(vls.begin(), vls.end(),[&mergedLSinVLS](LineSegment ls)->bool
                                    {
                                        if (find(mergedLSinVLS.begin(), mergedLSinVLS.end(),ls)!=mergedLSinVLS.end()) return true;
                                        else return false;
                                    }), vls.end());
                vls.push_back(LineSegment(ls1.p, ls2.p));
                i=0;j=1;
            }
        }
    }
    
    vls.erase(remove_if(vls.begin(), vls.end(), isShort), vls.end());
    
    return vls;
}

vector<LineSegment> Ransac::ransac(const vector<Point> &tmpPoints) {
    double threshold = 30.0;
    vector<LineSegment> lineSegments;
    vector<tuple<Point, int>> dataPoints;
    
    //(Point, visited)のタプルでvectorを構成（visited:まだ見てない=0,もう見た=1）
    for (int i=0; i<tmpPoints.size(); ++i) {
        dataPoints.push_back(make_tuple(tmpPoints[i], 0));
    }
    
    srand((unsigned)time(NULL));
    
    while(true) {
        
        int countAvairable = 0;
        for (int i=0; i<dataPoints.size(); ++i) {
            if (get<1>(dataPoints[i])==1) countAvairable += 1;
        }
        if (countAvairable==dataPoints.size()) break;
        
        int index;
        do {
            index = rand() % dataPoints.size();
        } while(get<1>(*(dataPoints.begin()+index))==1);
        
        Point basePoint = get<0>(*(dataPoints.begin()+index));
        double rho = 0;
        int maxInlier = 0;
        Line detectedLine = Line(0, 0);
        LineSegment ls = LineSegment(Point(0, 0), Point(0, 0));
        vector<tuple<Point, int>> inlierPoints;
        int inliers = 0;
        for (double deg=0; deg<180.0; deg+=0.25) {
            inliers = 0;
            vector<tuple<Point, int>> tmpInliers;
            Point forwardPoint = basePoint, postPoint = basePoint;
            double rad = DEG2RAD(deg);
            //直線の推定
            rho = basePoint.x * cos(rad) + basePoint.y * sin(rad);
            Line l = Line(rho, rad);
            tmpInliers.push_back(dataPoints[index]);
            //inlierの数の計算
            for (int i=index+1; i<dataPoints.size(); ++i) {
                if (l.distancePoint(get<0>(dataPoints[i])) < threshold
                    && get<1>(dataPoints[i])==0
                    && forwardPoint.distance(get<0>(dataPoints[i])) <= 500.0) {
                    inliers += 1;
                    tmpInliers.push_back(dataPoints[i]);
                    forwardPoint = get<0>(dataPoints[i]);
                } else {
                    break;
                }
            }
            for (int i=index-1; i>=0; --i) {
                if (l.distancePoint(get<0>(dataPoints[i])) < threshold
                    && get<1>(dataPoints[i])==0
                    && postPoint.distance(get<0>(dataPoints[i])) <= 500.0) {
                    inliers += 1;
                    tmpInliers.push_back(dataPoints[i]);
                    postPoint = get<0>(dataPoints[i]);
                } else {
                    break;
                }
            }
            
            if (inliers >= maxInlier) {
                maxInlier = inliers;
                inlierPoints.clear();
                copy(tmpInliers.begin(), tmpInliers.end(), back_inserter(inlierPoints));
                detectedLine = l;
                ls = LineSegment(l.getIntersectionOfPerpendicular(forwardPoint), l.getIntersectionOfPerpendicular(postPoint)); //lとの交差点を入れる
            }
        }
        //inlierが一番多くなる線分の抽出完了
        if (ls.abs() > 100 && maxInlier > 0) {
            lineSegments.push_back(ls);
        }
        for (int i=0; i<inlierPoints.size(); ++i) {
            for (int j=0; j<dataPoints.size(); ++j) {
                if (get<0>(inlierPoints[i]).distance(get<0>(dataPoints[j]))==0) {
                    get<1>(dataPoints[j]) = 1;
                }
            }
        }
    }
    merge(lineSegments, 300.0);
    return lineSegments;
}

void Ransac::merge(vector<LineSegment> &vls, double threshold) {
    vector<LineSegment> mergedLS;
    mergedLS.clear();
    
    for (int i=0; i<vls.size(); ++i) {
        for (int j=i+1; j<vls.size(); ++j) {
            mergedLS.clear();
            LineSegment ls1 = vls[i], ls2 = vls[j];
            if (ls1 > ls2) {
                LineSegment tmpLs = ls1;
                ls1 = ls2;
                ls2 = tmpLs;
            }
            if (max(Line(ls2).distancePoint(ls1.center()), Line(ls1).distancePoint(ls2.center())) >  threshold) continue;
            double angleDiff = abs(RAD2DEG(ls1.angle()) - RAD2DEG(ls2.angle()));
            
            if (angleDiff < 15.0) {
                if (max(RAD2DEG(ls1.angle()), RAD2DEG(ls2.angle()))>90 && min(RAD2DEG(ls1.angle()), RAD2DEG(ls2.angle()))<=90) {
                    if (RAD2DEG(ls1.angle())>90.0) {
                        LineSegment tmpLs = ls1;
                        ls1 = ls2;
                        ls2 = tmpLs;
                    }
                    Line l = Line(ls2);
                    Point p1, p2;
                    p1 = l.getIntersectionOfPerpendicular(ls1.p);
                    p2 = l.getIntersectionOfPerpendicular(ls1.q);
                    if ((ls2.p <= p1 && p1 < ls2.q) && (p2 < ls2.p)) {
                        //qq
                        LineSegment ls = LineSegment(ls1.q, ls2.q);
                        mergedLS.push_back(ls1);
                        mergedLS.push_back(ls2);
                        removeLSFromVector(vls, mergedLS);
                        vls.push_back(ls);
//                        cout << ls << endl;
                        i = 0; j = 1;
                    } else if ((p1 > ls2.q) && (ls2.p <= p2 && p2 < ls2.q)) {
                        //pp
                        LineSegment ls = LineSegment(ls1.p, ls2.p);
                        mergedLS.push_back(ls1);
                        mergedLS.push_back(ls2);
                        removeLSFromVector(vls, mergedLS);
                        vls.push_back(ls);
//                        cout << ls << endl;
                        i = 0; j = 1;
                    } else if ((ls2.p <= p1 && p1 < ls2.q) && (ls2.p <= p2 && p2 < ls2.q)) {
                        //remove ls1
                        mergedLS.push_back(ls1);
                        removeLSFromVector(vls, mergedLS);
                        i = 0; j = 1;
                    } else if ((p1 <= ls2.p) && (p2 >= ls2.q)) {
                        //remove ls2
                        mergedLS.push_back(ls2);
                        removeLSFromVector(vls, mergedLS);
                        i = 0; j = 1;
                    } else if ((p1 <= ls2.p) && (p2 <= ls2.p) && (ls1.p.distance(ls2.p)<500.0)) {
                        //qq
                        LineSegment ls = LineSegment(ls1.q, ls2.q);
                        mergedLS.push_back(ls1);
                        mergedLS.push_back(ls2);
                        removeLSFromVector(vls, mergedLS);
                        vls.push_back(ls);
//                        cout << ls << endl;
                        i = 0; j = 1;
                        
                    } else if ((p1 >= ls2.q) && (p2 >= ls2.q) && (ls1.q.distance(ls2.q)<500.0)) {
                        //pp
                        LineSegment ls = LineSegment(ls1.p, ls2.p);
                        mergedLS.push_back(ls1);
                        mergedLS.push_back(ls2);
                        removeLSFromVector(vls, mergedLS);
                        vls.push_back(ls);
//                        cout << ls << endl;
                        i = 0; j = 1;
                    }
                    //            特別枠
                } else if (max(RAD2DEG(ls1.angle()), RAD2DEG(ls2.angle()))<=45 || min(RAD2DEG(ls1.angle()), RAD2DEG(ls2.angle()))>=135) {
                    Line l = Line(ls2);
                    Point p1, p2;
                    p1 = l.getIntersectionOfPerpendicular(ls1.p);
                    p2 = l.getIntersectionOfPerpendicular(ls1.q);
                    if ((p1 < ls2.p) && (ls2.p <= p2 && p2 < ls2.q)) {
                        LineSegment ls = LineSegment(ls1.p, ls2.q);
                        mergedLS.push_back(ls1);
                        mergedLS.push_back(ls2);
                        removeLSFromVector(vls, mergedLS);
                        vls.push_back(ls);
//                        cout << ls << endl;
                        i = 0; j = 1;
                    } else if ((ls2.p <= p1 && p1 < ls2.q) && (ls2.p <= p2 && p2 < ls2.q)) {
                        mergedLS.push_back(ls1);
                        removeLSFromVector(vls, mergedLS);
                        i = 0; j = 1;
                    } else if ((p1 < ls2.p) && (p2 <= ls2.p) && (ls1.q.distance(ls2.p)<500.0)) {
                        LineSegment ls = LineSegment(ls1.p, ls2.q);
                        mergedLS.push_back(ls1);
                        mergedLS.push_back(ls2);
                        removeLSFromVector(vls, mergedLS);
                        vls.push_back(ls);
//                        cout << ls << endl;
                        i = 0; j = 1;
                    }
                } else {
                    if (ls1.q.y > ls2.q.y) {
                        LineSegment tmpLs = ls1;
                        ls1 = ls2;
                        ls2 = tmpLs;
                    }
                    Line l = Line(ls2);
                    Point p1, p2;
                    p1 = l.getIntersectionOfPerpendicular(ls1.p);
                    p2 = l.getIntersectionOfPerpendicular(ls1.q);
                    if ((ls2.p <= p1 && p1 < ls2.q) && (ls2.p <= p2 && p2 < ls2.q)) {
                        mergedLS.push_back(ls1);
                        removeLSFromVector(vls, mergedLS);
                        i = 0; j = 1;
                    } else if ((p1 < ls2.p) && (ls2.p <= p2 && p2 < ls2.q)) {
                        LineSegment ls = LineSegment(ls1.p, ls2.q);
                        mergedLS.push_back(ls1);
                        mergedLS.push_back(ls2);
                        removeLSFromVector(vls, mergedLS);
                        vls.push_back(ls);
//                        cout << ls << endl;
                        i = 0; j = 1;
                    } else if ((p2 > ls2.q) && (ls2.p <= p1 && p1 < ls2.q)) {
                        LineSegment ls = LineSegment(ls1.q, ls2.p);
                        mergedLS.push_back(ls1);
                        mergedLS.push_back(ls2);
                        removeLSFromVector(vls, mergedLS);
                        vls.push_back(ls);
//                        cout << ls << endl;
                        i = 0; j = 1;
                    } else if ((p1 <= ls2.p) && (p2 >= ls2.q)){
                        mergedLS.push_back(ls2);
                        removeLSFromVector(vls, mergedLS);
                        i = 0; j = 1;
                    } else if ((p1 <= ls2.p) && (p2 <= ls2.p) && (ls1.q.distance(ls2.p)<500.0)) {
                        LineSegment ls = LineSegment(ls1.p, ls2.q);
                        mergedLS.push_back(ls1);
                        mergedLS.push_back(ls2);
                        removeLSFromVector(vls, mergedLS);
                        vls.push_back(ls);
//                        cout << ls << endl;
                        i = 0; j = 1;
                    } else if ((p1 >= ls2.q) && (p2 >= ls2.q) && (ls1.p.distance(ls2.q)<500.0)) {
                        LineSegment ls = LineSegment(ls1.q, ls2.p);
                        mergedLS.push_back(ls1);
                        mergedLS.push_back(ls2);
                        removeLSFromVector(vls, mergedLS);
                        vls.push_back(ls);
//                        cout << ls << endl;
                        i = 0; j = 1;
                    }
                }
            } else if (angleDiff > 160.0) {
                Line l = Line(ls2);
                Point p1, p2;
                p1 = l.getIntersectionOfPerpendicular(ls1.p);
                p2 = l.getIntersectionOfPerpendicular(ls1.q);
                if ((p1 < ls2.p) && (ls2.p <= p2 && p2 < ls2.q)) {
                    LineSegment ls = LineSegment(ls1.p, ls2.q);
                    mergedLS.push_back(ls1);
                    mergedLS.push_back(ls2);
                    removeLSFromVector(vls, mergedLS);
                    vls.push_back(ls);
//                    cout << ls << endl;
                    i = 0; j = 1;
                } else if ((ls2.p <= p1 && p1 < ls2.q) && (ls2.p <= p2 && p2 < ls2.q)) {
                    mergedLS.push_back(ls1);
                    removeLSFromVector(vls, mergedLS);
                    i = 0; j = 1;
                } else if ((p1 < ls2.p) && (p2 <= ls2.p) && (ls1.q.distance(ls2.p)<500.0)) {
                    LineSegment ls = LineSegment(ls1.p, ls2.q);
                    mergedLS.push_back(ls1);
                    mergedLS.push_back(ls2);
                    removeLSFromVector(vls, mergedLS);
                    vls.push_back(ls);
//                    cout << ls << endl;
                    i = 0; j = 1;
                }
            }
        }
    }
    vls.erase(remove_if(vls.begin(), vls.end(), [](LineSegment ls)->bool{
        if (ls.abs() < 500.0) return true;
        else return false;
    }), vls.end());
}

void Ransac::removeLSFromVector(vector<LineSegment> &vls, vector<LineSegment> &merged) {
    vls.erase(remove_if(vls.begin(), vls.end(), [&merged](LineSegment l)->bool{
        if (find(merged.begin(), merged.end(), l)!=merged.end()) return true;
        else return false;
    }), vls.end());
}

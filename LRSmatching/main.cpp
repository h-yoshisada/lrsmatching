//
//  main.cpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/06/13.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <algorithm>

#include "affine_map.hpp"
#include "constant.hpp"
#include "environment.hpp"
#include "line.hpp"
#include "line_segment.hpp"
#include "matching.hpp"
#include "open_data_file.hpp"
#include "point.hpp"
#include "ransac.hpp"
#include "structure.hpp"
#include "evaluation.hpp"
#include "gnuplot.hpp"
#include "background_detection.hpp"
#include "groundtruth.hpp"
#include "graph.hpp"
#include "geos_sample.hpp"

using namespace std;

int main(int argc, const char * argv[]) {

//    各データの表示など
//
    /*
    vector<vector<Point>> vPoints;
    vector<Point> allPoints, lidars;
    vector<vector<LineSegment>> vLineSegments;
    vector<LineSegment> allLineSegments;
    for (int i=1; i<=15; ++i) {
       if (i==11) continue;
        char filePath[100];
        sprintf(filePath, FILE_ALIBABA_FORMAT, i);
        vector<double> data = OpenDataFile::readCSV(filePath);
        vector<Point> points = OpenDataFile::coordinateTransformation(data);
        points.erase(remove_if(points.begin(), points.end(), [](Point p)->bool{
            if (p.distance(Point(0, 0))> 25000) return true;
            return false;
        }), points.end());
        Gnuplot gp;
        gp.draw(points);
        gp.end();
        AffineMap base = alibabaMap[0];
        base.inverse();
        vector<Point> tmpPoints;
        for (Point p: points) {
            tmpPoints.push_back(base.transform(alibabaMap[i-1].transform(p)));
            allPoints.push_back(alibabaMap[i-1].transform(p));
        }
        lidars.push_back(alibabaMap[i-1].transform(Point(0, 0)));
        vPoints.push_back(tmpPoints);
    }
    Gnuplot gp;
    gp.draw(allPoints, lidars);
    gp.end();
    exit(1);
     */
////
    vector<double> data;
    vector<Point> points, keypoints;
    vector<vector<Point>> groups;
    vector<LineSegment> ls;
    char filePath[100];
    sprintf(filePath, FILE_ALIBABA_FORMAT, 15);
    data = OpenDataFile::readCSV(filePath);
    points = OpenDataFile::coordinateTransformation(data);
    points.erase(remove_if(points.begin(), points.end(), [](Point p)->bool{
        if (p.distance(Point(0, 0))> 25000) return true;
        return false;
    }), points.end());
    Gnuplot gp1;
    gp1.draw(points);
    gp1.end();
    groups = Ransac::grouping(points);
    Gnuplot gp2;
    gp2.draw(groups);
    cout << "number of Groups : " << groups.size() << endl;
    gp2.end();
    for (vector<Point> vp : groups) {
        vector<LineSegment> vls = Ransac::ransac(vp);
        copy(vls.begin(), vls.end(), back_inserter(ls));
    }
    Gnuplot gp3;
    cout << "number of Line Segments : " << ls.size() << endl;
    gp3.draw(points, ls);
    gp3.end();
    keypoints = KeyPoint::calcKeyPoints(points);
    cout << "number of KeyPoints : " << keypoints.size() << endl;
    Gnuplot gp4;
    gp4.draw(points, keypoints);
    gp4.end();

    exit(1);

//    int lrs;
//    cout << "Input Lrs num : ";
//    cin >> lrs;
//    BackGroundDetection::bgDetection(lrs);
//    exit(0);
    
//    vector<Point3D> points = OpenDataFile::coordinateTransformation(data, 5, 1000);
//    points.erase(remove_if(points.begin(), points.end(), [](Point3D p)->bool {
//        if (p.distance3D(Point3D(0, 0, 1000)) > 25000) return true;
//        return false;
//    }), points.end());
//    ofstream ofs;
//    ofs.open(OUTPUT_POINT_CLOUD);
//    for (int i=0; i<points.size(); ++i) {
//        ofs << points[i].x << " " << points[i].y << " " << points[i].z << endl;
//    }
//    ofs.close();
//    
//
//    points.erase(remove_if(points.begin(), points.end(), [](Point p)->bool{
//        if (p.distance(Point(0, 0))> 25000) return true;
//        return false;
//    }), points.end());


    //線分とキーポイントを用いた位置合わせ
    
    vector<int> vLrsNum;
    vector<tuple<int, double, double>> vLrsInfo;
    vector<pair<int, int>> vLrsPairs;
    cout << "data place list" << endl;
    cout << "[i] : 5th floor of IST." << endl;
    cout << "[a] : office of alibaba in China." << endl;
    cout << "[k] : a drug store in Kyoto." << endl;
    string command;
    int mode = -1;
    do {
        cout << "input Command : ";
        cin >> command;
        if (command=="i") mode = 0;
        else if (command=="a") mode = 1;
        else if (command=="k") mode = 2;
        else cout << "invalid command." << endl;
    } while (mode==-1);

    cout << "Input the number of LIDAR > ";
    int num;
    cin >> num;
    for (int i=1; i<=num; ++i) {
        cout << "LIDAR " << i << " > ";
        int lrs;
        cin >> lrs;
        vLrsNum.push_back(lrs);
    }
    
//    cout << "Input the number of base LIDAR : LIDAR ";
//    int base;
//    cin >> base;
    
    
//    Graph graph = Graph(vLrsNum);
//    graph.makeEdges();
//    Evaluation::evaluation(graph, mode);

    //    for (int i=1; i<=num; ++i) {
//        int lrs;
//        double height, angle;
//        cout << "Sensor number  " << i << " > ";
//        cin >> lrs;
//        cout << "Height of sensor " << i << " [mm] > ";
//        cin >> height;
//        cout << "Angle of sensor " << i << " [deg] (-90 ~ +90) > ";
//        cin >> angle;
//        vLrsInfo.push_back(make_tuple(lrs, height, angle));
//        vLrsNum.push_back(lrs);
//    }
    
//    for (int i=0; i<vLrsNum.size(); ++i) {
//        for (int j=i+1; j<vLrsNum.size(); ++j) {
//            if (i==j) continue;
//            vLrsPairs.push_back(make_pair(vLrsNum[i], vLrsNum[j]));
////            vLrsPairs.push_back(make_pair(vLrsNum[j], vLrsNum[i]));
//        }
//    }
//    Evaluation::evaluation(vLrsNum, vLrsPairs, mode);


    int matchingNum;
    cout << "Enter the number of combinations you want to match > ";
    cin >> matchingNum;
    for (int i=0; i<matchingNum; ++i) {
        int lrs1, lrs2;
        cout << "combination " << i+1 << " : LIDAR 1 > ";
        cin >> lrs1;
        cout << "combination " << i+1 << " : LIDAR 2 > ";
        cin >> lrs2;
        if (find(vLrsNum.begin(), vLrsNum.end(), lrs1)!=vLrsNum.end() && find(vLrsNum.begin(), vLrsNum.end(), lrs2)!=vLrsNum.end()) {
            vLrsPairs.push_back(make_pair(lrs1, lrs2));
        } else {
            cout << "Data not exist. Input again." << endl;
            i-=1;
        }
    }

    Evaluation::evaluation(vLrsNum, vLrsPairs, mode);
//
    
}



//
//  line.hpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/06/13.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#ifndef line_hpp
#define line_hpp

#include <algorithm>
#include <cmath>
#include "constant.hpp"
#include "line_segment.hpp"
#include "point.hpp"

using namespace std;

class LineSegment;

class Line {
public:
    // theta is radian
    double rho, theta;
    
    Line (double rho, double theta) {
        this->rho = rho;
        this->theta = theta;
    }
    
    Line (const LineSegment &ls);
    
    // y = ax + b
    double a() const {
        return -1.0 / tan(theta);
    }
    double b() const {
        return rho / sin(theta);
    }
    
    // to_string
    // <Line: rho = *, theta = *[rad] / *[deg], y = *x + *>
    friend std::ostream& operator << (std::ostream &os, const Line& l) {
        os << "<Line: rho = " << l.rho << ", theta = " << l.theta << "[rad] / " << RAD2DEG(l.theta) << "[deg], " << "y = " << l.a() << "x + " << l.b() << ">";
        return os;
    }
    
    // pointから引いた垂線との交点を返すメソッド
    Point getIntersectionOfPerpendicular(const Point& p) const {
        double rho_0 = p.x * sin(theta) - p.y * cos(theta);
        
        double x = rho * cos(theta) + rho_0 * sin(theta);
        double y = rho * sin(theta) - rho_0 * cos(theta);
        
        return Point(x, y);
    }
    
    // pointとの距離を返すメソッド
    double distancePoint(const Point& p) const {
        return abs(p.x * cos(theta) + p.y * sin(theta) - rho);
    }
    
    // theta, x, yからrhoを計算する静的メソッド
    static double calcRho(double theta, const Point &p) {
        return p.x * cos(theta) + p.y * sin(theta);
    }
};

#endif /* line_hpp */

//
//  line_segment.cpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/06/13.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#include "line_segment.hpp"
#include <iostream>

// (x - p) : (q - x) = a : b となる内分点xを返す
Point LineSegment::internallyDividingPoint(double a, double b) {
    double x = (a * q.x + b * p.x) / (a+b);
    double y = (a * q.y + b * p.y) / (a+b);
    return Point(x, y);
}

// 線分の両端をlenずつ短くした線分を返す
// もとの線分の長さが2*len以下ならば，p, qが中点の長さ0の線分を返す
LineSegment LineSegment::makeSmaller(double len) const {
    if (abs() <= 2 * len) {
        return LineSegment(center(), center());
    }
    
    double angle = this->angle();
    if (angle >= M_PI_2) angle -= M_PI;
    Point newP(p.x + len * cos(angle), p.y + len * sin(angle));
    Point newQ(q.x - len * cos(angle), q.y - len * sin(angle));
    
    return LineSegment(newP, newQ);
}

bool LineSegment::checkIntersected(const LineSegment& ls) const {
    double ta = (ls.p.x - ls.q.x) * (p.y - ls.p.y) + (ls.p.y - ls.q.y) * (ls.p.x - p.x);
    double tb = (ls.p.x - ls.q.x) * (q.y - ls.p.y) + (ls.p.y - ls.q.y) * (ls.p.x - q.x);
    double tc = (p.x - q.x) * (ls.p.y - p.y) + (p.y - q.y) * (p.x - ls.p.x);
    double td = (p.x - q.x) * (ls.q.y - p.y) + (p.y - q.y) * (p.x - ls.q.x);
    
    return tc * td <= 0 && ta * tb <= 0;
}

double LineSegment::similarity (const LineSegment& ls) const {
    double overlap_distance = fmin(this->overlap(ls), ls.overlap(*this));
    double segment_distance = max(
                                  Line(ls).distancePoint(this->center()),
                                  Line(*this).distancePoint(ls.center())
                                  );
    
    if (overlap_distance < TH_OVERLAP) return 0;
    if (segment_distance > TH_DIST) return 0;
    
    double min_length = fmin(ls.abs(), this->abs());
    
    return (overlap_distance / min_length) + ((TH_DIST - segment_distance) / TH_DIST);
}

double LineSegment::overlap (const LineSegment& ls) const {
    Line line1(*this);
    Point intersection1 = line1.getIntersectionOfPerpendicular(ls.p);
    Point intersection2 = line1.getIntersectionOfPerpendicular(ls.q);
    
    Point point1 = min(max(intersection1, intersection2), max(this->p, this->q));
    Point point2 = max(min(intersection1, intersection2), min(this->p, this->q));
    
    if (point1 <= point2) return 0;
    
    //    cout << "dis = " << point1.distance(point2) << endl;
    //    cout << "abs = " << ls.abs() << endl;
    //    double theta = ls.angle() - this->angle();
    //    if(theta < 0) theta *= -1;
    //    
    //    if(RAD2DEG(theta) > 10) return 0;
    
    return point1.distance(point2);
}

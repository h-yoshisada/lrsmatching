//
//  groundtruth.hpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/12/14.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#ifndef groundtruth_hpp
#define groundtruth_hpp

#include "constant.hpp"
#include "affine_map.hpp"

extern AffineMap ISTMap[16];
extern AffineMap alibabaMap[15];
extern AffineMap kyotoMap[15];

#endif /* groundtruth_hpp */

//
//  keypoint.hpp
//  LRSmatching
//
//  Created by Hikaru on 2017/08/29.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#ifndef keypoint_hpp
#define keypoint_hpp

#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <numeric>

#include "point.hpp"
#include "constant.hpp"

using namespace std;

class KeyPoint {
public:
    static vector<Point> calcKeyPoints(const vector<Point> &points);
    static vector<double> evaluationSimilarity(vector<Point> keypoints1, vector<Point> keyPoints2);
};

#endif /* keypoint_hpp */

//
//  gnuplot.hpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/06/13.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#ifndef gnuplot_hpp
#define gnuplot_hpp

#include <iostream>
#include <string>
#include <sstream>
#include <vector>

#include "constant.hpp"
#include "environment.hpp"
#include "line_segment.hpp"
#include "point.hpp"
#include "point3D.hpp"

using namespace std;

class Gnuplot {
private:
    FILE *gp;
    void multiple_plot(const vector<Point> &vp);
    void multiple_plot(const vector<LineSegment> &vl);
    void multiple_plot(const vector<Point3D> &vp);
    void resize(const vector<vector<Point>> vps, const vector<vector<LineSegment>> vls);
    void set3Dmode();
    void end3Dmode();
    
public:
    Gnuplot();
    void setTitle(const string);
    void setSize(const int minx, const int miny, const int maxx, const int maxy);
    void setOutputEps(const string);
    void draw(const vector<Point> &vp);
    void draw(const vector<Point> &vp1, const vector<Point> &vp2);
    void draw(const vector<LineSegment> &vp);
    void draw(const vector<LineSegment> &vp1, const vector<LineSegment> &vp2);
    void draw(const vector<Point> &vp, const vector<LineSegment> &vl);
    void draw(const vector<Point> &vp, const vector<Point> &vp2, const vector<LineSegment> &vl);
    void draw(const vector<Point> &vp1, const vector<Point> &vp2, const vector<LineSegment> &vl1, const vector<LineSegment> &vl2);
    void draw(const vector< vector<Point> > &vps);
    void draw(const vector<Point3D> &vp);
    void draw(const vector<Point3D> &vp1, const vector<Point3D> &vp2);
    void draw(const vector<vector<Point>> &vps, const vector<vector<LineSegment>> &vls);
    void draw(const string fileName);
    void drawPolygon(const vector<Point> &vp);
    void drawPolygon(const vector<Point> &vp1, const vector<Point> &vp2);
    void end();
};

#endif /* gnuplot_hpp */

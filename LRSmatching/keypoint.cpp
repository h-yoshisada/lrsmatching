//
//  keypoint.cpp
//  LRSmatching
//
//  Created by Hikaru on 2017/08/29.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#include "keypoint.hpp"
#include "gnuplot.hpp"


vector<Point> KeyPoint::calcKeyPoints(const vector<Point> &points) {
    vector<Point> keyPoints, clusterPoints;
    clusterPoints.clear();
    vector<vector<Point>> cls;
    for (int i=0; i<points.size()-1; ++i) {
        if (abs(points[i].distance(points[i+1])) > 800 || i == points.size()-2) {
            if (clusterPoints.size() >= 10) {
                keyPoints.push_back(clusterPoints[0]);
                Point prev = clusterPoints[0];
                for (int j=2; j<clusterPoints.size()-2; ++j) {
                    Point vec1, vec2;
                    vec1 = Point(clusterPoints[j-2].x - clusterPoints[j].x, clusterPoints[j-2].y - clusterPoints[j].y);
                    vec2 = Point(clusterPoints[j+2].x - clusterPoints[j].x, clusterPoints[j+2].y - clusterPoints[j].y);
                    double innerProduct, crossProduct;
                    innerProduct = vec1.x * vec2.x + vec1.y * vec2.y;
                    crossProduct = vec1.x * vec2.y - vec1.y * vec2.x;
                    double theta = atan2(crossProduct, innerProduct);
                    if (RAD2DEG(theta) >= -115 && RAD2DEG(theta) <= 115) {
                            keyPoints.push_back(clusterPoints[j]);
                            prev = clusterPoints[j];
                    }
                }
                keyPoints.push_back(clusterPoints[clusterPoints.size()-1]);
            }
            if (clusterPoints.size() > 0) {
                cls.push_back(clusterPoints);
            }
            clusterPoints.clear();
        } else {
            clusterPoints.push_back(points[i]);
        }
    }
    
//    Gnuplot gp;
//    gp.draw(points, keyPoints);
//    gp.end();
    return keyPoints;
}

vector<double> KeyPoint::evaluationSimilarity(vector<Point> keypoints1, vector<Point> keyPoints2) {
    Point matchPoint;
    vector<double> distanceVector;
    for (Point p: keypoints1) {
        double tmpDistance = INF;
        for (Point q: keyPoints2) {
            double distance = p.distance(q);
            if (distance < tmpDistance) {
                tmpDistance = distance;
                matchPoint = q;
            }
        }
        if (tmpDistance < 50) {
            distanceVector.push_back(tmpDistance);
            keyPoints2.erase(remove_if(keyPoints2.begin(), keyPoints2.end(),
                                       [&matchPoint](Point p)->bool {
                                           if (p == matchPoint) return true;
                                           else return false;
                                       }), keyPoints2.end());
        }
    }
    return distanceVector;
}

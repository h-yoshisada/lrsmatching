//
//  background_detection.cpp
//  LRSmatching
//
//  Created by Hikaru on 2017/11/21.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#include "background_detection.hpp"

void BackGroundDetection::bgDetection(int lrsNum) {
    char filename[100];
    sprintf(filename, FILE_KYOTO_ANIMATION_FORMAT, lrsNum);
    vector<vector<double>> dataVector(1081, vector<double>(0, 0));
    int percentile_25 = (int) MAX_SIZE / 4;
    int percentile_75 = (int) MAX_SIZE * 3 / 4;
    int mean = (int) MAX_SIZE / 2;
    string backgroundPointsFileName = "backgroundPoints.plt";
    FILE *fp = fopen(backgroundPointsFileName.c_str(), "w");
    
    array<double, ARRAY_SIZE> maxArray, minArray, averageArray, deviationArray;
    for (int i=0; i<ARRAY_SIZE; ++i) {
        maxArray[i] = 0;
        minArray[i] = INF;
        averageArray[i] = 0;
        deviationArray[i] = 0;
    }
    
    vector<double> datas;
    string dataLine;
    string field;
    ifstream ifs(filename);
    
    if (ifs.fail()) {
        cerr << "Error: Can't find the data file." << endl;
        exit(1);
    }
    vector<Point> backgroundPoints;
    
    Gnuplot gp;
    Gnuplot gp_bg;
    gp_bg.setSize(-15000, -15000, 15000, 15000);
    gp.setSize(-15000, -15000, 15000, 15000);
    int indexOfLine = 0;
    backgroundPoints.clear();
    string timestamp="";
    auto itr = dataVector.begin();
    while (getline(ifs, dataLine)) {
        datas.clear();
        istringstream stream(dataLine);
        int count = 0;
        itr = dataVector.begin();
        while (getline(stream, field, ',')) {
            if(count==0) {
                timestamp = to_string(stold(field));
            } else {
                if (stoi(field)==0) continue;
//                if (stoi(field)<minArray[count-1]) minArray[count-1]=stoi(field);
//                if (stoi(field)>maxArray[count-1]) maxArray[count-1]=stoi(field);
//                double ave = ((count-1) * averageArray[count-1] + stoi(field)) / count;
//                double dev = ((count-1) * (pow(averageArray[count-1], 2)+deviationArray[count-1]) + pow(stoi(field), 2)) / count - pow(ave, 2);
//                averageArray[count-1] = ave;
//                deviationArray[count-1] = dev;
                //                dataArray[count][indexOfLine] = stoi(field);
                itr->push_back(stoi(field));
                ++itr;
                datas.push_back(stoi(field));
            }
            count++;
        }
//        cout << "indexOfLine = " << indexOfLine << endl;
        
        if (indexOfLine==MAX_SIZE-1) {
            fp = fopen(backgroundPointsFileName.c_str(), "w");
            backgroundPoints.clear();
            
            for (int i=0; i<dataVector.size(); ++i) {
                double rad = DEG2RAD(-135+i*0.25);
                vector<double> v = dataVector[i];
                sort(v.begin(), v.end());
                double diff = v[percentile_75] - v[percentile_25];
                if ((v[mean] <= 10000 && diff <= 60) || (v[mean] <= 30000 && diff <= 100)) {
                    Point p(v[mean]*cos(rad),v[mean]*sin(rad));
                    backgroundPoints.push_back(p);
                    fprintf(fp, "%f %f\n", p.x, p.y);
                }
            }
            fclose(fp);
            
//            for (int i=0; i<ARRAY_SIZE; ++i) {
//                double param = -135 + i*0.25;
//                double deviation = sqrt(deviationArray[i]);
//                cout << deviation << endl;
//                if (deviation < 5 * averageArray[i]) {
//                    double data = datas[i];
//                    backgroundPoints.push_back(Point(data*cos(DEG2RAD(param)), data*sin(DEG2RAD(param))));
//                }
//                maxArray[i] = 0;
//                minArray[i] = INF;
//                averageArray[i] = 0;
//                deviationArray[i] = 0;
//            }
            gp_bg.setTitle("timestamp : " + timestamp + " , backgroundSize : " + to_string(backgroundPoints.size()));
            gp_bg.draw(backgroundPointsFileName);
            dataVector.clear();
            dataVector = vector<vector<double>>(1081, vector<double>(0, 0));
        }
        
        indexOfLine = (indexOfLine + 1) % MAX_SIZE;
        vector<Point> points = OpenDataFile::coordinateTransformation(datas);
        //usleep(25000);
        gp.setTitle("timestamp : " + timestamp);
        gp.draw(points);
    }
    gp_bg.end();
    gp.end();
}

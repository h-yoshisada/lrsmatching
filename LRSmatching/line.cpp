//
//  line.cpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/06/13.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#include "line.hpp"

Line::Line (const LineSegment &ls) {
    this->theta = ls.angle() + M_PI / 2;
    this->rho = Line::calcRho(this->theta, ls.p);
}

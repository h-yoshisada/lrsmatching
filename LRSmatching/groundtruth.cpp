//
//  groundtruth.cpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/12/14.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#include "groundtruth.hpp"

AffineMap kyotoMap[15] = {
    AffineMap(DEG2RAD(318), -12214, -2910),
    AffineMap(0, 0, 0),
    AffineMap(DEG2RAD(228), -7563, -482),
    AffineMap(DEG2RAD(307), -6899, -716),
    AffineMap(DEG2RAD(225), -695, -716),
    AffineMap(DEG2RAD(270), -1949, -2910),
    AffineMap(DEG2RAD(315), -4677, -7712),
    AffineMap(DEG2RAD(320), -7109, -7712),
    AffineMap(DEG2RAD(313), -9578, -7712),
    AffineMap(DEG2RAD(25), -11308, -8350),
    AffineMap(DEG2RAD(325), -10638, -10966),
    AffineMap(DEG2RAD(50), -10615, -15550),
    AffineMap(DEG2RAD(220), -6824, -12430),
    AffineMap(DEG2RAD(130), -58, -15560),
    AffineMap(DEG2RAD(180), -1861, -10461)
};
AffineMap alibabaMap[15] = {
    AffineMap(DEG2RAD(90), 2890, 0),
    AffineMap(DEG2RAD(90), 6360, 0),
    AffineMap(DEG2RAD(90), 11380, 0),
    AffineMap(DEG2RAD(89), 15100, 0),
    AffineMap(DEG2RAD(0), 17800, -20),
    AffineMap(DEG2RAD(180), 23500, -20),
    AffineMap(DEG2RAD(87), 27690, 0),
    AffineMap(DEG2RAD(90), 32125, 0),
    AffineMap(DEG2RAD(90), 36885, 0),
    AffineMap(DEG2RAD(2), 34650, -7400),
    AffineMap(DEG2RAD(135), 43000, 2000),
    AffineMap(DEG2RAD(134), 48020, 100),
    AffineMap(DEG2RAD(90), 54170, 100),
    AffineMap(DEG2RAD(45), 59800, 3250),
    AffineMap(DEG2RAD(45), 68530, 3250)
};

AffineMap ISTMap[16] = {
    AffineMap(DEG2RAD(  0),   220,     0),
    AffineMap(DEG2RAD(  0),   210, -5395),
    AffineMap(DEG2RAD(  0),   220,-10930),
    AffineMap(DEG2RAD(285),   160,  6300),
    AffineMap(DEG2RAD(335),   160,  6300),
    AffineMap(DEG2RAD(357),   210, 14140),
    AffineMap(DEG2RAD(357),   210, 20360),
    AffineMap(DEG2RAD(135),  1470, 13610),
    AffineMap(DEG2RAD(217),  1470,  4150),
    AffineMap(DEG2RAD(180),  2050,  1850),
    AffineMap(DEG2RAD(274), -1590,  4080),
    AffineMap(DEG2RAD(  0), -5910,  2160),
    AffineMap(DEG2RAD(182),  4100, -4760),
    AffineMap(DEG2RAD( 90),  1000,-17770),
    AffineMap(DEG2RAD(270),   400, 24250),
    AffineMap(DEG2RAD(179),  1580,  7220)
};

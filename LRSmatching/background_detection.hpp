//
//  background_detection.hpp
//  LRSmatching
//
//  Created by Hikaru on 2017/11/21.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#ifndef background_detection_hpp
#define background_detection_hpp

#include <iostream>
#include <vector>
#include <array>
#include <unistd.h>

#include "constant.hpp"
#include "open_data_file.hpp"
#include "gnuplot.hpp"

using namespace std;

class BackGroundDetection {
public:
    static void bgDetection (int lrsNum);
    
};


#endif /* background_detection_hpp */

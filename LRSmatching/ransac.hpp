//
//  ransac.hpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/06/13.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#ifndef ransac_hpp
#define ransac_hpp

#include <iostream>
#include <vector>
#include <random>
#include <set>
#include <limits>
#include <cmath>
#include <fstream>
#include <algorithm>
#include <ctime>
#include <cstdlib>

#include "line_segment.hpp"
#include "gnuplot.hpp"

using namespace std;

class Ransac{
private:
    static Line estimateLine(const vector<Point>& points);
    
public:
    static vector<LineSegment> ransacAlgorithm(const vector<Point> &points);
    static vector<LineSegment> mergeLineSegments(vector<LineSegment> vls, double threshold);
    static vector<vector<Point>> grouping(const vector<Point> &points);
    static vector<LineSegment> ransac(const vector<Point> &tmpPoints);
    static void merge(vector<LineSegment> &vls, double threshold);
    static void removeLSFromVector(vector<LineSegment> &vls, vector<LineSegment> &merged);

};

#endif /* ransac_hpp */

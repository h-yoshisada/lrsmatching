//
//  structure.hpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/06/13.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#ifndef structure_hpp
#define structure_hpp

struct Result {
    double sim;
    double angle;
    double trans_x;
    double trans_y;
    double keyPoint_x;
    double keyPoint_y;
    double lineSegmentSililarity;
    double keyPointSimilarity;
    int matchingMethod; //0:line segments, 1:key points
    bool operator<( const Result& right ) const {
        return sim > right.sim;
    }
};

struct KeyMatching {
    double sumDistance;
    double angle;
    double trans_x;
    double trans_y;
    double keyPoint_x;
    double keyPoint_y;
    bool operator<(const KeyMatching& right) const {
        return sumDistance < right.sumDistance;
    }
};

#endif /* structure_hpp */

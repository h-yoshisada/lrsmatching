//
//  open_data_file.hpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/06/13.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#ifndef open_data_file_hpp
#define open_data_file_hpp

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <cmath>

#include "environment.hpp"
#include "constant.hpp"
#include "point.hpp"
#include "point3D.hpp"

using namespace std;

class OpenDataFile {
private:
    OpenDataFile();
public:
    static vector<double> readCSV(string filename);
    static vector<Point> coordinateTransformation(vector<double> &data);
    static vector<Point3D> coordinateTransformation(vector<double> &data, double angle, double height);
};

#endif /* open_data_file_hpp */

//
//  matching.hpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/06/13.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#ifndef matching_hpp
#define matching_hpp

#include <iostream>
#include <cmath>
#include <vector>
#include <string>
#include <map>
#include <fstream>
#include <tuple>

#include "affine_map.hpp"
#include "constant.hpp"
#include "gnuplot.hpp"
#include "line.hpp"
#include "line_segment.hpp"
#include "point.hpp"
#include "structure.hpp"
#include "keypoint.hpp"

using namespace std;

//static class
class Matching {
public:
    static double calc_sim(vector<LineSegment> lines1, vector<LineSegment> lines2);
    static tuple<double, double, double> calc_sim_tuple(vector<LineSegment> lines1, vector<LineSegment> lines2, vector<Point> keyPoints1, vector<Point> keyPoints2);
    
public:
    static vector<vector<LineSegment>> clustering(vector<LineSegment> lines);
    static void matching(const vector<LineSegment> lines1, const vector<LineSegment> lines2, vector<Point> keyPoints1, vector<Point> keyPoints2, int lrs_1, int lrs_2, vector<Result> &results);
    static void matchingLineSegmentsThread(const vector<LineSegment> lines1, const vector<LineSegment> lines2, vector<Point> keyPoints1, vector<Point> keyPoints2, int lrs_1, int lrs_2, int startIndex, int endIndex, vector<Result> &results);
    static void matchingKeyPoints(const vector<LineSegment> lines1, const vector<LineSegment> lines2, vector<Point> keyPoints1, vector<Point> keyPoints2, int lrs_1, int lrs_2, vector<Result> &results);
    static void matchingKeyPointsThread(const vector<LineSegment> lines1, const vector<LineSegment> lines2, vector<Point> keyPoints1, vector<Point> keyPoints2, int lrs_1, int lrs_2, int startIndex, int endIndex, vector<Result> &results);
    static bool checkConsistency(const vector<LineSegment> lines1, const vector<LineSegment> lines2, const AffineMap affineMap);
    static bool checkConsistency(const vector<LineSegment> lines1, const vector<LineSegment> lines2, const Point lrs1Position);

};

#endif /* matching_hpp */

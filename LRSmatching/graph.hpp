//
//  graph.hpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/12/28.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#ifndef graph_hpp
#define graph_hpp

#include <iostream>
#include <vector>

#include "structure.hpp"
#include "constant.hpp"
#include "affine_map.hpp"

using namespace std;

class Graph {
public:
    vector<vector<int>> adjacencyMatrix;
    vector<vector<int>> visitedMatrix;
    vector<vector<AffineMap>> resultMatrix;
    vector<vector<double>> weightMatrix;
    vector<int> lrsNumberList;
    
    Graph(const vector<int> &lrsNum);
    
    void makeEdges(const vector<pair<int, int>> &vLrsPairs);
    void makeEdges();
    
};

#endif /* graph_hpp */

//
//  evaluation.hpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/07/31.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#ifndef evaluation_hpp
#define evaluation_hpp

#include <iostream>
#include <vector>
#include <sstream>
#include <string>
#include <cmath>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <algorithm>
#include <chrono>
#include <thread>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/registration/icp.h>
#include <pcl/search/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>

#include "affine_map.hpp"
#include "constant.hpp"
#include "environment.hpp"
#include "line.hpp"
#include "line_segment.hpp"
#include "matching.hpp"
#include "open_data_file.hpp"
#include "point.hpp"
#include "point3D.hpp"
#include "ransac.hpp"
#include "structure.hpp"
#include "keypoint.hpp"
#include "groundtruth.hpp"
#include "graph.hpp"
#include "geos_sample.hpp"

using namespace std;

//static class
class Evaluation {
public:
    static void evaluate(vector<int> vLrsNum);
    static void evaluation(vector<int> sensors, vector<pair<int, int>> vLrsPairs, int place);
    static void evaluation(vector<tuple<int, double, double>> sensorsInfo, vector<pair<int, int>> vLrsPairs, int place);
    static void evaluation(Graph graph, int place);
    static Result positionEstimation (int lrs1, int lrs2, vector<Point> &dataPoints1, vector<Point> &dataPoints2);
    static AffineMap evaluation_ICP(vector<Point> dataPoints1, vector<Point> dataPoints2);
private:
    static void evaluate(vector<int> vLrsNum, vector<string> vFilePath, vector<AffineMap> vAffineMap);
//    static void print4x4Matrix (const Eigen::Matrix4d& matrix);
};

#endif /* evaluation_hpp */

//
//  geos_sample.hpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2018/01/13.
//  Copyright © 2018年 Hikaru Yoshisada. All rights reserved.
//

#ifndef geos_sample_hpp
#define geos_sample_hpp

#include <iostream>
#include <vector>

#include <geos/geom/Polygon.h>
#include <geos/geom/LinearRing.h>
#include <geos/geom/CoordinateSequenceFactory.h>
#include <geos/geom/GeometryFactory.h>

#include "point.hpp"

class GeosSample : public geos::geom::GeometryFactory {
public:
    geos::geom::Polygon* makePolygon(const std::vector<Point> &points, Point p);
    geos::geom::Geometry* calculateIntersection(geos::geom::Geometry* LrsPolygon1, geos::geom::Geometry* LrsPolygon2);
};

#endif /* geos_sample_hpp */

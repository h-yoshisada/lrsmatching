//
//  constant.hpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/06/13.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#ifndef constant_hpp
#define constant_hpp

#include <cmath>

#define RAD2DEG(rad) ((rad) / M_PI * 180.0)
#define DEG2RAD(deg) ((deg) / 180.0 * M_PI)
#define INF 100000000
#define LINESEGMENT_MATCHING 0
#define KEYPOINT_MATCHING 1
#define NUM_THREAD_KEYPOINT 4
#define NUM_THREAD_LINESEGMENTS 1
#define NUM_DATALINE 1000

#define DELTA 0.0001
#define MAX_SIZE 400
#define ARRAY_SIZE 1081



#endif /* constant_hpp */

//
//  environment.hpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/06/13.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#ifndef environment_hpp
#define environment_hpp

// マシン毎に定数を設定するファイル
// このファイルはコミットしないこと！
// gnuplotのパス
#define GNUPLOT_PATH "/usr/local/bin/gnuplot -persist"

#define FILE_FORMAT "csv/LRSData%02d.csv"
#define FILE_ALIBABA_FORMAT "alibabaData/alibaba-%02d.csv"
#define FILE_KYOTO_FORMAT "kyotoData/kyoto_%02d.csv"
#define FILE_KYOTO_ANIMATION_FORMAT "kyotoData/kyoto_%02d_2nd.csv"
#define PLOT_FORMAT "plot/LRSData%02d.plt"
#define OUTPUTFILEDIST "distance_error_%04d%02d%02d.csv"
#define OUTPUTFILEANGLE "angle_error_%04d%02d%02d.csv"
#define OUTPUTLOG "matchingEvaluation_%04d%02d%02d.log"
#define OUTPUTDATAPLOT_EPS "plot/LRSdata_%02d.eps"
#define OUTPUT_POINT_CLOUD "pointcloud.txt"

#endif /* environment_hpp */

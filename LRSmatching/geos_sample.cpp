//
//  geos_sample.cpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2018/01/13.
//  Copyright © 2018年 Hikaru Yoshisada. All rights reserved.
//

#include "geos_sample.hpp"

geos::geom::Polygon* GeosSample::makePolygon(const std::vector<Point> &points, Point p) {
    geos::geom::CoordinateSequence *tmp = getCoordinateSequenceFactory()->create((size_t)0, 0);
    tmp->add(geos::geom::Coordinate(p.x, p.y));
    for (int i=0; i<points.size(); ++i) {
        tmp->add(geos::geom::Coordinate(points[i].x, points[i].y));
    }
    tmp->add(geos::geom::Coordinate(p.x, p.y));
    geos::geom::LinearRing *shell = createLinearRing(tmp);
    return createPolygon(shell, NULL);
}

geos::geom::Geometry* GeosSample::calculateIntersection(geos::geom::Geometry* LrsPolygon1, geos::geom::Geometry* LrsPolygon2) {
    return LrsPolygon2->intersection(LrsPolygon1);
    //self-intersection???
    
}

//
//  point.hpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/06/13.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#ifndef point_hpp
#define point_hpp

#include <cmath>
#include <ostream>

class Point {
public:
    double x, y;
    
public:
    Point() {
        this->x = this->y = 0;
    }
    
    Point(double x, double y) {
        this->x = x;
        this->y = y;
    }
    
    Point clone() const {
        return Point(x, y);
    }
    
    // 2点間の距離
    double distance(Point p) const {
        return sqrt(pow(p.x - x, 2) + pow(p.y - y, 2));
    }
    
    // to_string
    friend std::ostream& operator << (std::ostream &os, const Point& p) {
        os << "(" << p.x << ", " << p.y << ")";
        return os;
    }
    
    // 比較演算子
    bool operator == (const Point& p) const {
        return compare(p) == 0;
    }
    
    bool operator > (const Point& p) const {
        return compare(p) == +1;
    }
    
    bool operator < (const Point& p) const {
        return compare(p) == -1;
    }
    
    bool operator != (const Point& p) const {
        return compare(p) != 0;
    }
    
    bool operator >= (const Point& p) const {
        return compare(p) >= 0;
    }
    
    bool operator <= (const Point& p) const {
        return compare(p) <= 0;
    }
    
private:
    int compare (const Point& p) const {
        if (this->x == p.x) {
            if (this->y > p.y) return +1;
            else if (this->y < p.y) return -1;
            else return 0;
        } else {
            if (this->x > p.x) return +1;
            else return -1;
        }
    }
};

#endif /* point_hpp */

//
//  evaluation.cpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/07/31.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#include "evaluation.hpp"

void Evaluation::evaluate(vector<int> vLrsNum, vector<string> vFilePath, vector<AffineMap> vAffineMap) {
    int numDatas = (int) vLrsNum.size();
    vector<double> *data = new vector<double>[numDatas];
    vector<Point> *points = new vector<Point>[numDatas];
    vector<LineSegment> *lineSegments = new vector<LineSegment>[numDatas];
    vector< vector<Point> > tmpVectorPoints;
    vector<Result> matchingResult0to2, matchingResult1to2;
    
    //vLrsNum[0]とvLrsNum[1]の位置関係が既知とする．
    //この時にvLrsNum[1]の座標系の点をvLrsNum[0]の座標系に移すアフィン行列を実験のワールド座標系におけるアフィン行列から算出
    AffineMap baseAffine = vAffineMap[0];
    baseAffine.inverse();
    baseAffine.timeOfAffineMaps(vAffineMap[1]);
    cout << "baseAffine : " << baseAffine << endl;
    
    //データの読み込み
    for (int i=0; i<numDatas; ++i) {
        data[i] = OpenDataFile::readCSV(vFilePath[i]);
        points[i] = OpenDataFile::coordinateTransformation(data[i]);
    }
    
    tmpVectorPoints.push_back(points[0]);
    
    //線分検出
    for (int i=0; i<numDatas; ++i) {
        lineSegments[i] = Ransac::ransacAlgorithm(points[i]);
    }
    
    //クラスタリング
    vector<vector<LineSegment>> * clusteredLineSegments = new vector<vector<LineSegment>>[numDatas];
    for (int i=0; i<numDatas; ++i) {
        clusteredLineSegments[i] = Matching::clustering(lineSegments[i]);
    }
    
//    matchingResult0to2 = Matching::matching(clusteredLineSegments[0], clusteredLineSegments[2],  vLrsNum[0], vLrsNum[2]);
//    matchingResult1to2 = Matching::matching(clusteredLineSegments[1], clusteredLineSegments[2], vLrsNum[1], vLrsNum[2]);

    cout << "Matching result of LRS" << vLrsNum[0] << " and LRS" << vLrsNum[2] << endl;
    for (Result r: matchingResult0to2) {
        cout << r.sim << endl;
    }
//    matchingResult0to2.erase(remove_if(matchingResult0to2.begin(), matchingResult0to2.end(), [&vLrsNum](Result r){return r.base_LRS==vLrsNum[2];}), matchingResult0to2.end());
    
    for (int i=0; i<matchingResult0to2.size(); ++i) {
        AffineMap resultAffine0to2 = AffineMap(matchingResult0to2[i].angle, matchingResult0to2[i].trans_x, matchingResult0to2[i].trans_y);
        {
            vector<Point> tmpPoints;
            for (Point p: points[2]) {
                tmpPoints.push_back(resultAffine0to2.transform(p));
            }
            Gnuplot gp;
            gp.draw(points[0], tmpPoints);
            gp.end();
        }
        Point LRSPosition = resultAffine0to2.transform(Point(0, 0));
        double LRSangle = RAD2DEG(resultAffine0to2.angle());
        cout << "sim = " << matchingResult0to2[i].sim << endl;
        cout << resultAffine0to2 << endl;
        cout << "LRS : " << LRSPosition << ", angle = " << LRSangle << endl;
    }
    
    cout << endl << "Matching result of LRS" << vLrsNum[1] << " and LRS" << vLrsNum[2] << endl;
    for (Result r: matchingResult1to2) {
        cout << r.sim << endl;
    }
//    matchingResult1to2.erase(remove_if(matchingResult1to2.begin(), matchingResult1to2.end(), [&vLrsNum](Result r){return r.base_LRS==vLrsNum[2];}), matchingResult1to2.end());
    
    for (int i=0; i<matchingResult1to2.size(); ++i) {
        AffineMap resultAffine1to2 = AffineMap(matchingResult1to2[i].angle, matchingResult1to2[i].trans_x, matchingResult1to2[i].trans_y);
        {
            vector<Point> tmpPoints;
            for (Point p: points[2]) {
                tmpPoints.push_back(resultAffine1to2.transform(p));
            }
            Gnuplot gp;
            gp.draw(points[1], tmpPoints);
            gp.end();
        }
        Point LRSPosition = baseAffine.transform(resultAffine1to2.transform(Point(0, 0)));
        double LRSangle = RAD2DEG(baseAffine.angle()) + RAD2DEG(resultAffine1to2.angle());
        cout << "sim = " << matchingResult1to2[i].sim << endl;
        cout << resultAffine1to2 << endl;
        cout << "LRS : " << LRSPosition << ", angle = " << LRSangle << endl;
    }
    
    double maxDistance = INF;
    double maxAngle = INF;
    int tmpI = 0, tmpJ = 0;
    for (int i=0; i<matchingResult0to2.size(); ++i) {
        AffineMap affine0to2 = AffineMap(matchingResult0to2[i].angle, matchingResult0to2[i].trans_x, matchingResult0to2[i].trans_y);
        Point LRSPosition1 = affine0to2.transform(Point(0, 0));
        double LRSangle1 = RAD2DEG(affine0to2.angle());
        if (LRSangle1 <= -180) {
            LRSangle1 += 360;
        } else if (LRSangle1 > 180) {
            LRSangle1 -= 360;
        }
        for (int j=0; j<matchingResult1to2.size(); ++j) {
            AffineMap affine1to2 = AffineMap(matchingResult1to2[j].angle, matchingResult1to2[j].trans_x, matchingResult1to2[j].trans_y);
            Point LRSPosition2 = baseAffine.transform(affine1to2.transform(Point(0, 0)));
            double LRSangle2 = RAD2DEG(baseAffine.angle()) + RAD2DEG(affine1to2.angle());
            if (LRSangle2 <= -180) {
                LRSangle2 += 360;
            } else if (LRSangle2 > 180) {
                LRSangle2 -= 360;
            }
            //evaluation
            {
                double distance = LRSPosition1.distance(LRSPosition2);
                double angle = abs(LRSangle1 - LRSangle2);
                if (distance <= maxDistance && angle <= maxAngle) {
                    maxDistance = distance;
                    maxAngle = angle;
                    tmpI = i;
                    tmpJ = j;
                }
            }
        }
    }
    if (maxDistance < 300 && maxAngle < 5) {
        vector<Point> tmpPoints, tmpPoints2;
        for (Point p: points[2]) {
            tmpPoints.push_back(AffineMap(matchingResult0to2[tmpI].angle, matchingResult0to2[tmpI].trans_x, matchingResult0to2[tmpI].trans_y).transform(p));
            tmpPoints2.push_back(baseAffine.transform(AffineMap(matchingResult1to2[tmpJ].angle, matchingResult1to2[tmpJ].trans_x, matchingResult1to2[tmpJ].trans_y).transform(p)));
        }
        vector<Point> vTmp;
        for (Point p: points[1]) {
            vTmp.push_back(baseAffine.transform(p));
        }
        tmpVectorPoints.push_back(vTmp);
        tmpVectorPoints.push_back(tmpPoints);
//        tmpVectorPoints.push_back(tmpPoints2);
        
        Gnuplot gpTmp;
        gpTmp.draw(tmpVectorPoints);
        gpTmp.end();

    } else {
        cout << "Matching failed. " << endl;
    }
    
}

void Evaluation::evaluate(vector<int> vLrsNum) {
    //    第2回実験時のアフィンマップ
    AffineMap worldMap[15] = {
        AffineMap(DEG2RAD(  0),   220,     0),
        AffineMap(DEG2RAD(  0),   210, -5395),
        AffineMap(DEG2RAD(  0),   220,-10930),
        AffineMap(DEG2RAD(285),   160,  6300),
        AffineMap(DEG2RAD(335),   160,  6300),
        AffineMap(DEG2RAD(357),   210, 14140),
        AffineMap(DEG2RAD(357),   210, 20360),
        AffineMap(DEG2RAD(135),  1470, 13610),
        AffineMap(DEG2RAD(217),  1470,  4150),
        AffineMap(DEG2RAD(180),  2050,  1850),
        AffineMap(DEG2RAD(274), -1590,  4080),
        AffineMap(DEG2RAD(  0), -5910,  2160),
        AffineMap(DEG2RAD(182),  4100, -4760),
        AffineMap(DEG2RAD( 90),  1000,-17770),
        AffineMap(DEG2RAD(270),   400, 24250),
    };
    //    //    第2回実験時のアフィンマップ
    //    AffineMap worldMap[15] = {
    //        AffineMap(DEG2RAD(  0),   220,     0),
    //        AffineMap(DEG2RAD(  0),   210, -5395),
    //        AffineMap(DEG2RAD(  0),   210,-10930),
    //        AffineMap(DEG2RAD(285),   160,  6300),
    //        AffineMap(DEG2RAD(332),   160,  6300),
    //        AffineMap(DEG2RAD(  0),   210, 14140),
    //        AffineMap(DEG2RAD(  0),   210, 20360),
    //        AffineMap(DEG2RAD(136),  1500, 13610),
    //        AffineMap(DEG2RAD(220),  1470,  4150),
    //        AffineMap(DEG2RAD(180),  2050,  1850),
    //        AffineMap(DEG2RAD(270), -1590,  4080),
    //        AffineMap(DEG2RAD(  0), -5910,  2160),
    //        AffineMap(DEG2RAD(180),  4020, -4760),
    //        AffineMap(DEG2RAD( 90),   730,-17770),
    //        AffineMap(DEG2RAD(270),   730, 24250),
    //    };
    //
    vector<string> vFilePath;
    vector<AffineMap> vAffineMap;
    for (int i=0; i<vLrsNum.size(); ++i) {
        char filePath[100];
        sprintf(filePath, FILE_KYOTO_FORMAT, vLrsNum[i]);
        string str = filePath;
        vFilePath.push_back(str);
        vAffineMap.push_back(worldMap[vLrsNum[i]-1]);
    }
    
    evaluate(vLrsNum, vFilePath, vAffineMap);
}

/***********************************************************************************************************************
 グラフからすべてのLiDARの位置関係を推定するメソッド
 入力：
 １．グラフ（ノードとエッジ，隣接行列などを持つ）
 ２．場所
 **********************************************************************************************************************/
void Evaluation::evaluation(Graph graph, int place) {
    int numOfSensors = (int) graph.lrsNumberList.size();
    
    //基準となるLRSの番号を入力させる．不適切ならやり直し．
    int baseLRS;
    for (;;) {
        cout << "Input base LRS number : ";
        cin >> baseLRS;
        if (find(graph.lrsNumberList.begin(), graph.lrsNumberList.end(), baseLRS)==graph.lrsNumberList.end()) {
            cout << "Invalid number.";
            continue;
        }
        break;
    }
    cout << baseLRS << endl;
    
    int baseLRSIndex = (int) distance(graph.lrsNumberList.begin(), find(graph.lrsNumberList.begin(), graph.lrsNumberList.end(), baseLRS));
    
    for (int i=0; i<graph.adjacencyMatrix.size(); ++i) {
        if (graph.adjacencyMatrix[i][baseLRSIndex]==1) graph.adjacencyMatrix[i][baseLRSIndex] = 0;
    }
    
    vector<vector<Point>> allDataPoints = vector<vector<Point>>(numOfSensors, vector<Point>());
    for (int i=0; i<numOfSensors; ++i) {
        char fileName[100];
        if (place==0) sprintf(fileName, FILE_FORMAT, graph.lrsNumberList[i]);
        if (place==1) sprintf(fileName, FILE_ALIBABA_FORMAT, graph.lrsNumberList[i]);
        if (place==2) sprintf(fileName, FILE_KYOTO_FORMAT, graph.lrsNumberList[i]);
        vector<double> data = OpenDataFile::readCSV(fileName);
        vector<Point> dataPoints = OpenDataFile::coordinateTransformation(data);
        dataPoints.erase(remove_if(dataPoints.begin(), dataPoints.end(), [](Point p)->bool{
            if (p.distance(Point(0, 0))> 25000) return true;
            return false;
        }), dataPoints.end());
        copy(dataPoints.begin(), dataPoints.end(), back_inserter(allDataPoints[i]));
    }
    
    for (int i=0; i<numOfSensors; ++i) {
        for (int j=0; j<numOfSensors; ++j) {
            if (graph.adjacencyMatrix[i][j] == 1) {
                int lrs1 = graph.lrsNumberList[i];
                int lrs2 = graph.lrsNumberList[j];
                vector<Point> dataPoints1 = allDataPoints[i];
                vector<Point> dataPoints2 = allDataPoints[j];
                Result rst = positionEstimation(lrs1, lrs2, dataPoints1, dataPoints2);
                AffineMap resultAffine = AffineMap(rst.angle, rst.trans_x, rst.trans_y);
                if (rst.matchingMethod==KEYPOINT_MATCHING) {
                    AffineMap tmpAffine = AffineMap(rst.angle, rst.keyPoint_x, rst.keyPoint_y);
                    resultAffine = AffineMap(0, rst.trans_x - rst.keyPoint_x, rst.trans_y - rst.keyPoint_y);
                    resultAffine.timeOfAffineMaps(tmpAffine);
                }
                vector<Point> transPoints;
                for (Point p: dataPoints2) {
                    transPoints.push_back(resultAffine.transform(p));
                }
                Gnuplot gp;
                gp.setTitle("LRS " + to_string(lrs1) + " and LRS " + to_string(lrs2) +" : Similarity = " + to_string(rst.sim) + ", (angle, trans_x, trans_y) = (" + to_string(RAD2DEG(rst.angle)) + ", " + to_string(rst.trans_x) + ", " + to_string(rst.trans_y) + "), matrhing method = " + to_string(rst.matchingMethod));
                gp.draw(dataPoints1, transPoints);
                gp.end();
                if (graph.weightMatrix[i][j] == 0) {
                    graph.weightMatrix[i][j] = rst.sim;
                    graph.resultMatrix[i][j].timeOfAffineMaps(resultAffine);
                }
                if (graph.weightMatrix[j][i] > graph.weightMatrix[i][j]) {
                    graph.weightMatrix[i][j] = graph.weightMatrix[j][i];
                    graph.resultMatrix[i].erase(graph.resultMatrix[i].begin() + j);
                    AffineMap inv = graph.resultMatrix[j][i];
                    inv.inverse();
                    graph.resultMatrix[i].insert(graph.resultMatrix[i].begin() + j, inv);
                }
                if (graph.weightMatrix[i][j] > graph.weightMatrix[j][i]) {
                    graph.weightMatrix[j][i] = graph.weightMatrix[i][j];
                    graph.resultMatrix[j].erase(graph.resultMatrix[j].begin() + i);
                    AffineMap inv = graph.resultMatrix[i][j];
                    inv.inverse();
                    graph.resultMatrix[j].insert(graph.resultMatrix[j].begin() + i, inv);
                }
            }
        }
    }
    for (vector<double> v : graph.weightMatrix) {
        for (double d : v) {
            cout << d << " ";
        }
        cout << endl;
    }
    for (int i=0; i<graph.resultMatrix.size(); ++i) {
        for (int j=0; j<graph.resultMatrix[i].size(); ++j) {
            cout << graph.resultMatrix[i][j] << endl;
        }
        cout << endl;
    }
    //ここから経路探索問題？？
    
    vector<int> visited;
    vector<int> prev = vector<int>(numOfSensors, 0);
    prev[baseLRSIndex] = baseLRSIndex;
    int now, next;
    now = baseLRSIndex;
    visited.push_back(baseLRSIndex);
    while(1) {
        if (visited.size() == numOfSensors) break;
        double max = -1;
        int maxIndex = -1;
        for (int i=0; i<graph.weightMatrix[now].size(); ++i) {
            if (graph.weightMatrix[now][i] > max && graph.adjacencyMatrix[now][i] == 1 && graph.visitedMatrix[now][i] == 0) {
                max = graph.weightMatrix[now][i];
                maxIndex = i;
            }
        }
        if (max <= 0) {
            now = prev[now];
            cout << "now = prev[now] : " << now << endl;
            continue;
        }
        if (find(visited.begin(), visited.end(), maxIndex) != visited.end()) {
            graph.visitedMatrix[now][maxIndex] = 1;
            cout << "Index " << maxIndex << " was visited." << endl;
            continue;
        }
        next = maxIndex;
        prev[next] = now;
        
        cout << "now = " << now << " , next = " << next << endl;
        
        if (now != baseLRSIndex && graph.adjacencyMatrix[baseLRSIndex][next] == 1) {
            int index = next;
            vector<Point> basePoints = allDataPoints[baseLRSIndex];
            vector<Point> dataPoints = allDataPoints[next];
            double sim_tmp = 0;
            while(1) {
                int preview = prev[index];
                AffineMap affine = graph.resultMatrix[preview][index];
                sim_tmp += graph.weightMatrix[preview][index];
                vector<Point> tmpPoints;
                for (Point p: dataPoints) {
                    tmpPoints.push_back(affine.transform(p));
                }
                dataPoints.clear();
                copy(tmpPoints.begin(), tmpPoints.end(), back_inserter(dataPoints));
                index = preview;
                if (preview == baseLRSIndex) break;
            }
            vector<Point> baseKeyPoints = KeyPoint::calcKeyPoints(basePoints);
            vector<Point> keyPoints = KeyPoint::calcKeyPoints(dataPoints);
            vector<vector<Point>> baseGroups = Ransac::grouping(basePoints);
            vector<LineSegment> baseLineSegments;
            for (vector<Point> vp : baseGroups) {
                vector<LineSegment> vls = Ransac::ransac(vp);
                copy(vls.begin(), vls.end(), back_inserter(baseLineSegments));
            }
            vector<vector<Point>> groups = Ransac::grouping(dataPoints);
            vector<LineSegment> lineSegments;
            for (vector<Point> vp : groups) {
                vector<LineSegment> vls = Ransac::ransac(vp);
                copy(vls.begin(), vls.end(), back_inserter(lineSegments));
            }
            double similarity, lineSegmentSimilarity, keyPointSimilarity;
            tie (similarity, lineSegmentSimilarity, keyPointSimilarity) = Matching::calc_sim_tuple(baseLineSegments, lineSegments, baseKeyPoints, keyPoints);
            Gnuplot gp;
            gp.setTitle("LRS " + to_string(baseLRS) + " and " + to_string(graph.lrsNumberList[next]) + " : Similarity = " + to_string(similarity) + ", weight = " + to_string(graph.weightMatrix[baseLRSIndex][next]));
            gp.draw(allDataPoints[baseLRSIndex], dataPoints);
            gp.end();
            similarity += sim_tmp;
            if (similarity > graph.weightMatrix[baseLRSIndex][next]) {
                graph.visitedMatrix[now][next] = 1;
                visited.push_back(next);
                now = next;
            } else {
                prev[next] = -1;
                graph.visitedMatrix[now][next] = 1;
            }
        } else {
            graph.visitedMatrix[now][next] = 1;
            visited.push_back(next);
            now = next;
        }
        
    }
    
    for (int i=0; i<prev.size(); ++i) {
        int index = i;
        vector<Point> dataPoints = allDataPoints[i];
        while(1) {
            int preview = prev[index];
            AffineMap affine = graph.resultMatrix[preview][index];
            vector<Point> tmpPoints;
            for (Point p: dataPoints) {
                tmpPoints.push_back(affine.transform(p));
            }
            dataPoints.clear();
            copy(tmpPoints.begin(), tmpPoints.end(), back_inserter(dataPoints));
            index = preview;
            if (preview == baseLRSIndex) break;
        }
        allDataPoints[i].clear();
        copy(dataPoints.begin(), dataPoints.end(), back_inserter(allDataPoints[i]));
    }
    for (int i=0; i<prev.size(); ++i) {
        cout << "preview[" << graph.lrsNumberList[i] << "] = " << graph.lrsNumberList[prev[i]] << endl;
    }
    
    Gnuplot gp;
    gp.setTitle("matchingResults");
    gp.draw(allDataPoints);
    gp.end();
}

/*****************
水平に対応した位置関係推定
 入力：
 LiDARセンサの集合，組み合わせの集合，計測場所
*****************/


void Evaluation::evaluation(vector<int> sensors, vector<pair<int, int> > vLrsPairs, int place) {
    
    /***ファイル出力用***/
    char buff[256];
    time_t timer = time(NULL);
    struct tm *date;
    if (timer==-1) {
        cout << "Error time" << endl;
        exit(1);
    }
    date = localtime(&timer);
    if (date==NULL) {
        cout << "Error date" << endl;
        exit(1);
    }
    strftime(buff, sizeof(buff), "eval_%Y%m%d_%H%M", date);
    if (mkdir(buff,S_IRWXU|S_IRGRP|S_IXGRP)==0) {
        cout << "Succeeded to make folder '" << buff << "'" << endl;
    } else {
        cout << "Failed to make folder '" << buff << "'" << endl;
        exit(1);
    }
    string dirname = string(buff);
    ofstream ofs;
    char file[256];
    sprintf(file, "/Evaluation_%s.csv", buff);
    string csvName = string(file);
    ofs.open(dirname+csvName);
    ofs << "baseLIDAR, target LIDAR, share rate, computational time , similarity ,  , ground truth ,  ,  , estimation(ICP) ,  , distance ,  , error(ICP) ,  ,  , estimation(proposed) ,  , distance ,  , error(proposed) ,  " << endl;
    ofs << " ,  ,  ,  ,  , x(m) , y(m) , angle(rad) , x , y , angle , distance(m) , x , y , angle , x , y , angle , distance , x , y , angle " << endl;
    /******/
    
    int numOfSensors = (int) sensors.size();
    //基準となるLRSの番号を取得
    int baseLrsNum = (*vLrsPairs.begin()).first;
    
    vector<int> preview = vector<int>(numOfSensors, 0);
    vector<AffineMap> affineMapVec = vector<AffineMap>(numOfSensors, AffineMap(0, 0, 0));
    int baseLRSIndex = (int) distance(sensors.begin(), find(sensors.begin(), sensors.end(), baseLrsNum));
    preview[baseLRSIndex] = baseLRSIndex;
    
    vector<vector<Point>> vAllPoints;
    vector<AffineMap> vAllAffineMaps;
    vector<Point> positionOfLidars;
    vAllPoints = vector<vector<Point>>(numOfSensors, vector<Point>());
    vAllAffineMaps = vector<AffineMap>(numOfSensors, AffineMap(0, 0, 0));
    for (int i=0; i<numOfSensors; ++i) {
        char fileName[100];
        if (place==0) sprintf(fileName, FILE_FORMAT, sensors[i]);
        if (place==1) sprintf(fileName, FILE_ALIBABA_FORMAT, sensors[i]);
        if (place==2) sprintf(fileName, FILE_KYOTO_FORMAT, sensors[i]);
        vector<double> data = OpenDataFile::readCSV(fileName);
        vector<Point> points = OpenDataFile::coordinateTransformation(data);
        points.erase(remove_if(points.begin(), points.end(), [](Point p)->bool{
            if (p.distance(Point(0, 0))> 25000) return true;
            return false;
        }), points.end());
        copy(points.begin(), points.end(), back_inserter(vAllPoints[i]));
        if (place==0) {
            vAllAffineMaps[i].timeOfAffineMaps(ISTMap[sensors[i]-1]);
        } else if (place==1) {
            vAllAffineMaps[i].timeOfAffineMaps(alibabaMap[sensors[i]-1]);
        } else if (place==2) {
            vAllAffineMaps[i].timeOfAffineMaps(kyotoMap[sensors[i]-1]);
        }
    }
    
    AffineMap baseAffine = vAllAffineMaps[distance(sensors.begin(), find(sensors.begin(), sensors.end(), baseLrsNum))];
    baseAffine.inverse();
    vector<vector<Point>> vPoints;
    
    cout << "LIDAR positions : " << endl;
    for (int i=0; i<numOfSensors; ++i) {
        Point p = baseAffine.transform(vAllAffineMaps[i].transform(Point(0, 0)));
        cout << vAllAffineMaps[i] << endl;
        positionOfLidars.push_back(p);
        vector<Point> tmpPoints;
        for (Point p: vAllPoints[i]) {
            tmpPoints.push_back(baseAffine.transform(vAllAffineMaps[i].transform(p)));
        }
        vPoints.push_back(tmpPoints);
        cout << "Position of LIDAR " << sensors[i] << " : " << p << endl;
    }
    sprintf(file, "/GroundTruth_Map.eps");
    string epsName = string(file);
    Gnuplot gpp;
    gpp.setOutputEps(dirname+epsName);
    gpp.draw(vPoints);
    gpp.end();
    
    
//    for (AffineMap ap : vAllAffineMaps) {
//        cout << ap << endl;
//    }
    
    for (pair<int, int> lrsPair: vLrsPairs) {
        
        auto start = chrono::system_clock::now();
        int lrs1 = lrsPair.first, lrs2 = lrsPair.second;
        vector<Point> dataPoints1, dataPoints2;
        dataPoints1 = vAllPoints[distance(sensors.begin(), find(sensors.begin(), sensors.end(), lrs1))];
        dataPoints2 = vAllPoints[distance(sensors.begin(), find(sensors.begin(), sensors.end(), lrs2))];
        preview[distance(sensors.begin(), find(sensors.begin(), sensors.end(), lrs2))] = (int) distance(sensors.begin(), find(sensors.begin(), sensors.end(), lrs1));
        AffineMap world1(0,0,0), world2(0,0,0);
        world1 = vAllAffineMaps[distance(sensors.begin(), find(sensors.begin(), sensors.end(), lrs1))];
        world2 = vAllAffineMaps[distance(sensors.begin(), find(sensors.begin(), sensors.end(), lrs2))];
        AffineMap inv = world1;
        inv.inverse();
        
        char outTitle[256];
        string fname;
        string outName;

        /**********/
        vector<vector<Point>> groupsDataPoints1, goupsGroundTruthPoints;
        groupsDataPoints1 = Ransac::grouping(dataPoints1);
        vector<Point> groundTruthPoints;
        for (Point p: dataPoints2) {
            groundTruthPoints.push_back(inv.transform(world2.transform(p)));
        }
        goupsGroundTruthPoints = Ransac::grouping(groundTruthPoints);
        vector<Point> tmpPoints1, tmpPoints2;
        for (vector<Point> vp: groupsDataPoints1) {
            copy(vp.begin(), vp.end(), back_inserter(tmpPoints1));
        }
        for (vector<Point> vp: goupsGroundTruthPoints) {
            copy(vp.begin(), vp.end(), back_inserter(tmpPoints2));
        }
        Point org = inv.transform(world2.transform(Point(0, 0)));
        
        Gnuplot gp_gt, gp_gt2;
//        gp_gt.setTitle("Ground Truth of matching : LIDAR" + to_string(lrs1) + " and LIDAR" + to_string(lrs2));
//        gp_gt.draw(dataPoints1, groundTruthPoints);
//        gp_gt.end();
        gp_gt2.setTitle("Ground Truth of matching : LIDAR" + to_string(lrs2) + " to LIDAR" + to_string(lrs1));
        sprintf(outTitle, "/GroundTruth-LIDAR%02d-to-LIDAR%02d.eps", lrs2, lrs1);
        fname = string(outTitle);
        outName = dirname + fname;
        gp_gt2.setOutputEps(outName);
        gp_gt2.draw(dataPoints1, groundTruthPoints);
        gp_gt2.end();
//        Gnuplot gnuplot;
//        gnuplot.drawPolygon(tmpPoints1, tmpPoints2);
//        gnuplot.end();
        GeosSample gs;
        geos::geom::Polygon* lrs1Polygon = gs.makePolygon(tmpPoints1, Point(0, 0));
        double areaLrs1 = lrs1Polygon->getArea() / 1000000;
        geos::geom::Polygon* lrs2Polygon = gs.makePolygon(tmpPoints2, org);
        double areaLrs2 = lrs2Polygon->getArea() / 1000000;
        geos::geom::Geometry* intersection = gs.calculateIntersection(lrs1Polygon, lrs2Polygon);
        double commonArea = intersection->getArea() / 1000000;
        cout << "LRS "<<lrs1<<" area = " << areaLrs1 << " [m^2] , LRS "<<lrs2<<" area = " << areaLrs2 << " [m^2] , commonArea = " << commonArea << " [m^2]" << endl;
        double commonRate;
        commonRate = commonArea / (areaLrs1 + areaLrs2 - commonArea) * 100;
        cout << "commonRate = " << commonRate << " [%] " << endl;
        if (commonRate == 0) continue;
        /**********/
        
        AffineMap ICPAffine = evaluation_ICP(dataPoints1, dataPoints2);
        vector<Point> tmpICPPoints;
        for (Point p: dataPoints2) {
            tmpICPPoints.push_back(ICPAffine.transform(p));
        }
        Gnuplot gp_ICP;
        string titleICP = "Result : " + to_string(lrs2) + " to " + to_string(lrs1) + " , (Angle, transX, transY) = (" + to_string(RAD2DEG(ICPAffine.angle())) + ", " + to_string(ICPAffine.trans_x()) + ", " + to_string(ICPAffine.trans_y()) + ")";
        gp_ICP.setTitle(titleICP);
        sprintf(outTitle, "/ICP-LIDAR%02d-to-LIDAR%02d.eps", lrs2, lrs1);
        fname = string(outTitle);
        outName = dirname + fname;
        gp_ICP.setOutputEps(outName);
        gp_ICP.draw(dataPoints1, tmpICPPoints);
        gp_ICP.end();

        Result rst = positionEstimation(lrs1, lrs2, dataPoints1, dataPoints2);
        AffineMap transAffine = AffineMap(rst.angle, rst.trans_x, rst.trans_y);
        if (rst.matchingMethod==KEYPOINT_MATCHING) {
            AffineMap tmpAffine = AffineMap(rst.angle, rst.keyPoint_x, rst.keyPoint_y);
            transAffine = AffineMap(0, rst.trans_x - rst.keyPoint_x, rst.trans_y - rst.keyPoint_y);
            transAffine.timeOfAffineMaps(tmpAffine);
        }

        
//        vAllAffineMaps[lrs1-1][lrs2-1] = transAffine;
        vector<Point> tmpPoints;
        for (Point p: dataPoints2) {
            tmpPoints.push_back(transAffine.transform(p));
        }
        auto end = chrono::system_clock::now();
        auto diff = end - start;
        stringstream stream;

        Point p(transAffine.trans_x(), transAffine.trans_y());
        
        Point gt = inv.transform(world2.transform(Point(0, 0)));
        double angleError = RAD2DEG(abs(rst.angle - (world2.angle() - world1.angle())));
        double errorVectorX = p.x - gt.x;
        double errorVectorY = p.y - gt.y;
        if (angleError > 180) angleError = 360 - angleError;
        double errorAngleICP = RAD2DEG(abs(ICPAffine.angle() - (world2.angle() - world1.angle())));
        if (errorAngleICP > 180) errorAngleICP = 360 - errorAngleICP;
        
//        if (gt.distance(p) > 500) exit(0);
        
        Gnuplot gp;
        string title = "Result : " + to_string(lrs2) + " to " + to_string(lrs1) + " , (Angle, transX, transY) = (" + to_string(RAD2DEG(transAffine.angle())) + ", " + to_string(transAffine.trans_x()) + ", " + to_string(transAffine.trans_y()) + ")";
        cout << title << endl;
        cout << "mode = " << rst.matchingMethod << endl;
        cout << "similarity = " << rst.sim << endl;
        stream << "[distance evaluation]" << endl;
        stream << "- world-LRS-2  = " << p   << endl;
        stream << "- Ground Truth = " << gt << endl;
        stream << "- error = " << gt.distance(p) << "[mm]" << endl;
        stream << "- error (vector) = (" << errorVectorX << ", " << errorVectorY << ")" << endl;
        //        stream << "- distance between 2 LRS = " << Point(0, 0).distance(p) << "[mm]" << endl;
        //        double distance = sqrt(pow(world1.trans_x()-world2.trans_x(), 2) + pow(world1.trans_y()-world2.trans_y(), 2));
        //
        //        stream << "- distance ground truth = " << distance << "[mm]" << endl;
        
        stream << "[angle evaluation]" << endl;
        stream << "- estimate relative angle        = " << RAD2DEG(rst.angle) << "[deg]" << endl;
        stream << "- ground truth of relative angle = " << RAD2DEG(world2.angle() - world1.angle()) << "[deg]" << endl;
        stream << "- error = " << angleError << "[deg]" << endl;
        cout << "LineSegmentSimilarity = " << rst.lineSegmentSililarity << ", keyPointSimilarity = " << rst.keyPointSimilarity << endl;
        cout << stream.str();
        cout << "elapsed time to matching LRS " << to_string(lrs1) << " and LRS " << to_string(lrs2) << " = " << chrono::duration_cast<chrono::seconds>(diff).count() <<" sec." << endl;
        gp.setTitle(title);

        sprintf(outTitle, "/Result-LIDAR%02d-to-LIDAR%02d.eps", lrs2, lrs1);
        fname = string(outTitle);
        outName = buff + fname;
        cout << outName << endl;
        gp.setOutputEps(dirname+fname);
        gp.draw(dataPoints1, tmpPoints);
        gp.end();
        affineMapVec[distance(sensors.begin(), find(sensors.begin(), sensors.end(), lrs2))].timeOfAffineMaps(transAffine);
//        vAllPoints[distance(sensors.begin(), find(sensors.begin(), sensors.end(), lrs2))].clear();
//        copy(tmpPoints.begin() , tmpPoints.end(), back_inserter(vAllPoints[distance(sensors.begin(), find(sensors.begin(), sensors.end(), lrs2))]));
        
        stringstream sstream;
        sstream << to_string(lrs1) << ", " << to_string(lrs2) << ", ";
        sstream << to_string(commonRate) << ", ";
        sstream << to_string(chrono::duration_cast<chrono::seconds>(diff).count()) << ", ";
        sstream << to_string(rst.sim) << ", ";
        sstream << to_string(gt.x / 1000) << ", " << to_string(gt.y / 1000) << ", " << to_string(RAD2DEG(world2.angle() - world1.angle())) << ", ";
        sstream << to_string(ICPAffine.trans_x() / 1000) << ", " << to_string(ICPAffine.trans_y() / 1000) << ", " << to_string(RAD2DEG(ICPAffine.angle())) << ", ";
        sstream << to_string(gt.distance(Point(ICPAffine.trans_x(), ICPAffine.trans_y())) / 1000) << ", ";
        sstream << to_string((ICPAffine.trans_x() - gt.x) / 1000) << ", " << to_string((ICPAffine.trans_y() - gt.y) / 1000) << ", " << to_string(errorAngleICP) << ", ";
        sstream << to_string(p.x / 1000) << ", " << to_string(p.y / 1000) << ", " << to_string(RAD2DEG(rst.angle)) << ", ";
        sstream << to_string(gt.distance(p) / 1000) << ", ";
        sstream << to_string(errorVectorX / 1000) << ", " << to_string(errorVectorY / 1000) << ", " << to_string(angleError) << endl;
        ofs << sstream.str() ;
        
    }
    ofs.close();
    vector<Point> estimatedLidarPositions;
    for (int i=0; i<numOfSensors; ++i) {
        int index = i;
        vector<Point> dataPoints = vAllPoints[i];
        Point org = Point(0, 0);
        while(1) {
            int prev = preview[index];
            AffineMap affine = affineMapVec[index];
            vector<Point> tmpPoints;
            for (Point p: dataPoints) {
                tmpPoints.push_back(affine.transform(p));
            }
            Point org_tmp = affine.transform(org);
            org = org_tmp;
            dataPoints.clear();
            copy(tmpPoints.begin(), tmpPoints.end(), back_inserter(dataPoints));
            index = prev;
            if (prev == baseLRSIndex) break;
        }
        vAllPoints[i].clear();
        copy(dataPoints.begin(), dataPoints.end(), back_inserter(vAllPoints[i]));
        estimatedLidarPositions.push_back(org);
    }
    for (int i=0; i<preview.size(); ++i) {
        cout << "preview[" << sensors[i] << "] = " << sensors[preview[i]] << endl;
    }
    for (int i=0; i<estimatedLidarPositions.size(); ++i) {
        Point groundTruth = positionOfLidars[i];
        Point estimated = estimatedLidarPositions[i];
        double errorX = estimated.x - groundTruth.x;
        double errorY = estimated.y - groundTruth.y;
        double dist = groundTruth.distance(Point(0, 0));
        double err = groundTruth.distance(estimated);
        double rate = err / dist * 100;

        cout << "Groundtruth position of LIDAR " << sensors[i] << " : " << groundTruth << endl;
        cout << "Estimated position of LIDAR " << sensors[i] << " : " << estimated << endl;
        cout << "  - Error of estimation : " << err << " [mm] " << endl;
        cout << "  - Error (vector) :    x = " << errorX << " , y = " << errorY << endl;
        cout << "  - Error (rate) : " << rate << " [%] " << endl << endl;
    }
    
    sprintf(file, "/Generated_Map.eps");
    epsName = string(file);
    Gnuplot gpl;
    gpl.setOutputEps(dirname + epsName);
    gpl.setTitle("matchingResults");
    gpl.draw(vAllPoints);
    gpl.end();
}
//"pcl::search::KdTree<pcl::PointXYZ, pcl::KdTreeFLANN<pcl::PointXYZ, flann::L2_Simple<float> > >::setPointRepresentation(boost::shared_ptr<pcl::PointRepresentation<pcl::PointXYZ> const> const&)", referenced from:
//  "pcl::search::KdTree<pcl::PointXYZ, pcl::KdTreeFLANN<pcl::PointXYZ, flann::L2_Simple<float> > >::KdTree(bool)", referenced from:
//clang: error: linker command failed with exit code 1 (use -v to see invocation)

/***********************************************************************************************************************
 点群から位置関係推定結果のアフィン行列を求めて返すメソッド
 入力：LRSの番号，LRSのデータ点群
 出力：アフィン行列
 仕様：一つ目の引数に指定した番号のLRSに対して二つ目の引数に指定したLRSを位置合わせする．
 **********************************************************************************************************************/
Result Evaluation::positionEstimation (int lrs1, int lrs2, vector<Point> &dataPoints1, vector<Point> &dataPoints2) {
    vector<LineSegment> lineSegments1, lineSegments2;
    vector<Point> keyPoints1, keyPoints2;
    //　一つ目のデータ
    vector<vector<Point>> groups1 = Ransac::grouping(dataPoints1);
    for (vector<Point> vp : groups1) {
        vector<LineSegment> vls = Ransac::ransac(vp);
        copy(vls.begin(), vls.end(), back_inserter(lineSegments1));
    }
//    Ransac::merge(lineSegments1, 500.0);
    keyPoints1 = KeyPoint::calcKeyPoints(dataPoints1);
    cout << "LRSData " << lrs1 << " : LineSegments = " << lineSegments1.size() << ", KeyPoints = " << keyPoints1.size() << endl;
    // 二つ目のデータ
    vector<vector<Point>> groups2 = Ransac::grouping(dataPoints2);
    for (vector<Point> vp : groups2) {
        vector<LineSegment> vls = Ransac::ransac(vp);
        copy(vls.begin(), vls.end(), back_inserter(lineSegments2));
    }
//    Ransac::merge(lineSegments2, 500.0);
    keyPoints2 = KeyPoint::calcKeyPoints(dataPoints2);
    cout << "LRSData " << lrs2 << " : LineSegments = " << lineSegments2.size() << ", KeyPoints = " << keyPoints2.size() << endl;
    
//    Gnuplot gp;
//    gp.draw(lineSegments1);
//    gp.end();
//    Gnuplot gp2;
//    gp2.draw(lineSegments2);
//    gp2.end();
    
    int index = (int) keyPoints1.size() / NUM_THREAD_KEYPOINT;
    int mod = keyPoints1.size() % NUM_THREAD_KEYPOINT;
    int index_ls = (int) lineSegments1.size() / NUM_THREAD_LINESEGMENTS;
    int mod_ls = lineSegments1.size() % NUM_THREAD_LINESEGMENTS;
    //マッチング
    vector<Result> results, results2;
    results.clear();
    results2.clear();
    vector<thread> threadsKeyPoints, threadsLineSegments;
    try {
        
        for (int i=0; i<NUM_THREAD_KEYPOINT; ++i) {
            threadsKeyPoints.push_back(thread(bind(&Matching::matchingKeyPointsThread, lineSegments1, lineSegments2, keyPoints1, keyPoints2, lrs1, lrs2, i*index, min((i+1)*index,(int)keyPoints1.size()), ref(results2))));
        }
        if (mod != 0) {
            threadsKeyPoints.push_back(thread(bind(&Matching::matchingKeyPointsThread, lineSegments1, lineSegments2, keyPoints1, keyPoints2, lrs1, lrs2, index*NUM_THREAD_KEYPOINT, keyPoints1.size(), ref(results2))));
        }
        for (int i=0; i<NUM_THREAD_LINESEGMENTS; ++i) {
            threadsLineSegments.push_back(thread(bind(&Matching::matchingLineSegmentsThread, lineSegments1, lineSegments2, keyPoints1, keyPoints2, lrs1, lrs2, i*index_ls, min((i+1)*index_ls, (int)lineSegments1.size()), ref(results))));
        }
        if (mod_ls != 0) {
            threadsLineSegments.push_back(thread(bind(&Matching::matchingLineSegmentsThread, lineSegments1, lineSegments2, keyPoints1, keyPoints2, lrs1, lrs2, index_ls*NUM_THREAD_LINESEGMENTS, lineSegments1.size(), ref(results))));
        }
        for (auto itr = threadsKeyPoints.begin(); itr != threadsKeyPoints.end(); ++itr) {
            itr->join();
        }
        for (auto itr = threadsLineSegments.begin(); itr != threadsLineSegments.end(); ++itr) {
            itr->join();
        }
    } catch (exception &ex) {
        cerr << ex.what() << endl;
    }
    copy(results2.begin(), results2.end(), back_inserter(results));
    sort(results.begin(), results.end());
    if (results.size() > 5) {
        results.erase(results.begin() + 5, results.end());
    }
    Result rst = *results.begin(); // the most high similarity result
    
    AffineMap affine = AffineMap(rst.angle, rst.trans_x, rst.trans_y);
//    vector<LineSegment> tmpLS = affine.transform(lineSegments2);
//    bool con = Matching::checkConsistency(lineSegments1, tmpLS, affine);
//    cout << "Consistency = " << con << endl;
    
    return rst;
}

/***********************************************************************************************************************
 3次元に対応した位置関係の推定を行うメソッド
 入力：
 1：各LRSの情報のvector (情報：番号，設置高さ(mm)，角度(-90 to 90, degree値))
 2：位置推定を行うLRSの番号の組（一番最初の組の1つ目のLRSが基準となる）
 3：LRSのデータを取った場所 (0:情報科学研究科, 1:アリババ, 2:薬局)
 **********************************************************************************************************************/
void Evaluation::evaluation(vector<tuple<int, double, double>> sensorsInfo, vector<pair<int, int>> vLrsPairs, int place) {
    //LRSの数を取得
    int numOfSensors = (int) sensorsInfo.size();
    //基準となるLRSの番号を取得
    int baseLrsNum = get<0>(*vLrsPairs.begin());
    vector<int> sensors;
    for (int i=0; i<numOfSensors; ++i) {
        sensors.push_back(get<0>(sensorsInfo[i]));
    }
    
    //全ての点群，線分群，アフィン行列の管理を行うvectorの宣言
    vector<vector<Point>> allDataPoints = vector<vector<Point>>(numOfSensors, vector<Point>());
    vector<vector<Point3D>> allDataPoints3D = vector<vector<Point3D>>(numOfSensors, vector<Point3D>());
    vector<AffineMap> allAffineMaps = vector<AffineMap>(numOfSensors, AffineMap(0, 0, 0));
    
    for (int i=0; i<numOfSensors; ++i) {
        char fileName[100];
        if (place==0) sprintf(fileName, FILE_FORMAT, get<0>(sensorsInfo[i]));
        if (place==1) sprintf(fileName, FILE_ALIBABA_FORMAT, get<0>(sensorsInfo[i]));
        if (place==2) sprintf(fileName, FILE_KYOTO_FORMAT, get<0>(sensorsInfo[i]));
        vector<double> data = OpenDataFile::readCSV(fileName);
        //3次元座標系に変換した点の集合が返ってくる
        vector<Point3D> points_3d = OpenDataFile::coordinateTransformation(data, get<2>(sensorsInfo[i]), get<1>(sensorsInfo[i]));
        points_3d.erase(remove_if(points_3d.begin(), points_3d.end(), [](Point3D p)->bool{
            if (p.distance2D(Point3D(0, 0, 0))> 25000) return true;
            return false;
        }), points_3d.end());
        vector<Point> points;
        for (Point3D p: points_3d) {
            points.push_back(Point(p.x, p.y));
        }
        copy(points.begin(), points.end(), back_inserter(allDataPoints[i]));
        copy(points_3d.begin(), points_3d.end(), back_inserter(allDataPoints3D[i]));
        if (place==0) {
            allAffineMaps[i].timeOfAffineMaps(ISTMap[sensors[i]-1]);
        } else if (place==1) {
            allAffineMaps[i].timeOfAffineMaps(alibabaMap[sensors[i]-1]);
        } else if (place==2) {
            allAffineMaps[i].timeOfAffineMaps(kyotoMap[sensors[i]-1]);
        }
    }
    
    AffineMap baseAffine = allAffineMaps[distance(sensors.begin(), find(sensors.begin(), sensors.end(), baseLrsNum))];
    
    for (pair<int, int> lrsPair: vLrsPairs) {
        auto start = chrono::system_clock::now();
        int lrs1 = lrsPair.first, lrs2 = lrsPair.second;
        vector<Point> dataPoints1, dataPoints2;
        
        dataPoints1 = allDataPoints[distance(sensors.begin(), find(sensors.begin(), sensors.end(), lrs1))];
        dataPoints2 = allDataPoints[distance(sensors.begin(), find(sensors.begin(), sensors.end(), lrs2))];
        
        Result rst = positionEstimation(lrs1, lrs2, dataPoints1, dataPoints2);
        AffineMap transAffine = AffineMap(rst.angle, rst.trans_x, rst.trans_y);
        if (rst.matchingMethod==KEYPOINT_MATCHING) {
            AffineMap tmpAffine = AffineMap(rst.angle, rst.keyPoint_x, rst.keyPoint_y);
            transAffine = AffineMap(0, rst.trans_x - rst.keyPoint_x, rst.trans_y - rst.keyPoint_y);
            transAffine.timeOfAffineMaps(tmpAffine);
        }
        
        auto end = chrono::system_clock::now();
        auto diff = end - start;
        stringstream stream;
        
        Point p(transAffine.trans_x(), transAffine.trans_y());
        AffineMap world1(0,0,0), world2(0,0,0);
        world1 = allAffineMaps[distance(sensors.begin(), find(sensors.begin(), sensors.end(), lrs1))];
        world2 = allAffineMaps[distance(sensors.begin(), find(sensors.begin(), sensors.end(), lrs2))];
        Point lrs2Position;
        lrs2Position = baseAffine.transform(p);
        
        double lrs1Height, lrs2Height;
        lrs1Height = get<1>(*(sensorsInfo.begin() + distance(sensors.begin(), find(sensors.begin(), sensors.end(), lrs1))));
        lrs2Height = get<1>(*(sensorsInfo.begin() + distance(sensors.begin(), find(sensors.begin(), sensors.end(), lrs2))));
        double lrs1Angle, lrs2Angle;
        lrs1Angle = get<2>(*(sensorsInfo.begin() + distance(sensors.begin(), find(sensors.begin(), sensors.end(), lrs1))));
        lrs2Angle = get<2>(*(sensorsInfo.begin() + distance(sensors.begin(), find(sensors.begin(), sensors.end(), lrs2))));
        
        Point3D lrs2Position3D;
        lrs2Position3D = Point3D(lrs2Position.x, lrs2Position.y, lrs2Height);
        
        Point gt(world2.trans_x(), world2.trans_y());
        double angleError = RAD2DEG(abs(rst.angle - (world2.angle() - world1.angle())));
        double errorVectorX = lrs2Position.x - gt.x;
        double errorVectorY = lrs2Position.y - gt.y;
        if (angleError > 180) angleError = 360 - angleError;
        
        vector<Point3D> dataPoints3D = allDataPoints3D[distance(sensors.begin(), find(sensors.begin(), sensors.end(), lrs2))];
        vector<Point3D> transedPoints3D;
        vector<Point> transedPoints;
        for (int i=0; i<dataPoints2.size(); ++i) {
            double pz = dataPoints3D[i].z;
            Point p = transAffine.transform(dataPoints2[i]);
            transedPoints.push_back(p);
            transedPoints3D.push_back(Point3D(p.x, p.y, pz));
        }
        
        allDataPoints[distance(sensors.begin(), find(sensors.begin(), sensors.end(), lrs2))].clear();
        copy(transedPoints.begin(), transedPoints.end(), back_inserter(allDataPoints[distance(sensors.begin(), find(sensors.begin(), sensors.end(), lrs2))]));
        allDataPoints3D[distance(sensors.begin(), find(sensors.begin(), sensors.end(), lrs2))].clear();
        copy(transedPoints3D.begin(), transedPoints3D.end(), back_inserter(allDataPoints3D[distance(sensors.begin(), find(sensors.begin(), sensors.end(), lrs2))]));

        string title = "matchingResult : " + to_string(lrs1) + " and " + to_string(lrs2) + ", similarity = " + to_string(rst.sim) + " , (Angle, transX, transY) = (" + to_string(RAD2DEG(transAffine.angle())) + ", " + to_string(transAffine.trans_x()) + ", " + to_string(transAffine.trans_y()) + ") , matchingMode = " + to_string(rst.matchingMethod);
        cout << title << endl;
        stream << "[distance evaluation]" << endl;
        stream << "- world-LRS-2  = " << lrs2Position   << endl;
        stream << "- Ground Truth = " << gt << endl;
        stream << "- error = " << gt.distance(lrs2Position) << "[mm]" << endl;
        stream << "- error (vector) = (" << errorVectorX << ", " << errorVectorY << ")" << endl;
        stream << "[angle evaluation]" << endl;
        stream << "- estimate relative angle        = " << RAD2DEG(rst.angle) << "[deg]" << endl;
        stream << "- ground truth of relative angle = " << RAD2DEG(world2.angle() - world1.angle()) << "[deg]" << endl;
        stream << "- error = " << angleError << "[deg]" << endl;
        cout << "LineSegmentSimilarity = " << rst.lineSegmentSililarity << ", keyPointSimilarity = " << rst.keyPointSimilarity << endl;
        cout << stream.str();

        cout << "elapsed time to matching LRS " << to_string(lrs1) << " and LRS " << to_string(lrs2) << " = " << chrono::duration_cast<chrono::seconds>(diff).count() <<" sec." << endl;
        
        Gnuplot gp;
        
    }
    
}


AffineMap Evaluation::evaluation_ICP(vector<Point> dataPoints1, vector<Point> dataPoints2) {
        
    //PCLで使えるように変換
    pcl::PointCloud<pcl::PointXYZ>::Ptr pc1(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr pc2(new pcl::PointCloud<pcl::PointXYZ>);
    
    for (Point p: dataPoints1) {
        pcl::PointXYZ pt;
        pt.x = p.x;
        pt.y = p.y;
        pt.z = 0;
        pc1->points.push_back(pt);
    }
    for (Point p: dataPoints2) {
        pcl::PointXYZ pt;
        pt.x = p.x;
        pt.y = p.y;
        pt.z = 0;
        pc2->points.push_back(pt);
    }
    
    pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
    icp.setInputSource(pc2);
    icp.setInputTarget(pc1);
    
    pcl::PointCloud<pcl::PointXYZ> result;
    icp.align(result);
    
    if (icp.hasConverged()) {
        cout << "converged. score = " << icp.getFitnessScore() << endl;
        Eigen::Matrix4f transformation = icp.getFinalTransformation();
        cout << transformation << endl;
        double cosine = transformation(0, 0);
        double sine = transformation(1, 0);
        double trans_x = transformation(0, 3);
        double trans_y = transformation(1, 3);
        AffineMap transAffine = AffineMap(cosine, sine, trans_x, trans_y);
        vector<Point> tmpPoints;
        for (Point p: dataPoints2) {
            tmpPoints.push_back(transAffine.transform(p));
        }
//        Gnuplot gp;
//        gp.draw(dataPoints1, tmpPoints);
//        gp.end();
        
        return transAffine;
    } else {
        cout << "Not converged." << endl;
        return AffineMap(0, 0, 0);
    }
    return AffineMap(0, 0, 0);

}



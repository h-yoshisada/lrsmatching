//
//  line_segment.hpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/06/13.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#ifndef line_segment_hpp
#define line_segment_hpp

#include <algorithm>
#include <cmath>

#include "line.hpp"
#include "point.hpp"

// 閾値(mm)
#define TH_DIST 300
#define TH_OVERLAP 300

using namespace std;

class Line;

class LineSegment {
public:
    // p <= q が成り立つようにする
    Point p, q;
    
    LineSegment(const Point& p, const Point& q) {
        if (p < q) {
            this->p = p.clone();
            this->q = q.clone();
        } else {
            this->p = q.clone();
            this->q = p.clone();
        }
    }
    
    // return [0, pi)
    double angle() const {
        double angle = atan2(dy(), dx());
        if (angle < 0) return angle + M_PI;
        return angle;
    }
    
    double dx() const { return p.x - q.x; }
    double dy() const { return p.y - q.y; }
    // 線分の長さ
    double abs() const { return sqrt(pow(dx(), 2.0) + pow(dy(), 2.0)); }
    
    // to_string
    friend std::ostream& operator << (std::ostream &os, const LineSegment& ls) {
        os << "<LineSegment: " << ls.p << " - " << ls.q << " : angle = " << RAD2DEG(ls.angle()) << "[deg]>";
        return os;
    }
    
    // 中点
    Point center() const {
        return Point((p.x + q.x) / 2.0, (p.y + q.y) / 2.0);
    }
    
    // (x - p) : (q - x) = a : b となる内分点xを返す
    Point internallyDividingPoint(double a, double b);
    
    // 線分の両端をlenずつ短くした線分を返す
    // もとの線分の長さが2*len以下ならば，p, qが中点の長さ0の線分を返す
    LineSegment makeSmaller(double len) const;
    
    // 線分の交差判定
    // trueが返れば交差してる
    bool checkIntersected(const LineSegment& ls) const;
    
    // 比較演算子
    bool operator == (const LineSegment& ls) const {
        return compare(ls) == 0;
    }
    
    bool operator > (const LineSegment& ls) const {
        return compare(ls) == +1;
    }
    
    bool operator < (const LineSegment& ls) const {
        return compare(ls) == -1;
    }
    
    bool operator != (const LineSegment& ls) const {
        return compare(ls) != 0;
    }
    
    bool operator >= (const LineSegment& ls) const {
        return compare(ls) >= 0;
    }
    
    bool operator <= (const LineSegment& ls) const {
        return compare(ls) <= 0;
    }
    
    // 線分の類似度を返すメソッド
    double similarity (const LineSegment& ls) const;
    double overlap (const LineSegment& ls) const;
    
private:
    int compare (const LineSegment& ls) const {
        if (this->q == ls.q) {
            if (this->p > ls.p) return +1;
            else if (this->p < ls.p) return -1;
            else return 0;
        } else {
            if (this->q > ls.q) return +1;
            else return -1;
        }
    }
    
};

#endif /* line_segment_hpp */

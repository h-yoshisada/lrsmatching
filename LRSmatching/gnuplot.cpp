//
//  gnuplot.cpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/06/13.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#include "gnuplot.hpp"

Gnuplot::Gnuplot() {
    gp = popen(GNUPLOT_PATH, "w");
}

void Gnuplot::resize(const vector<vector<Point>> vps, const vector<vector<LineSegment>> vls) {
    double maxX = -INF;
    double maxY = -INF;
    double minX = +INF;
    double minY = +INF;
    for(vector<Point> vp: vps) {
        for (Point p: vp) {
            maxX = max(maxX, p.x);
            maxY = max(maxY, p.y);
            minX = min(minX, p.x);
            minY = min(minY, p.y);
        }
    }
    
    for(vector<LineSegment> vl: vls) {
        for (LineSegment l: vl) {
            maxX = max(maxX, l.p.x);
            maxY = max(maxY, l.p.y);
            minX = min(minX, l.p.x);
            minY = min(minY, l.p.y);
            
            maxX = max(maxX, l.q.x);
            maxY = max(maxY, l.q.y);
            minX = min(minX, l.q.x);
            minY = min(minY, l.q.y);
        }
    }
    
    double width  = maxX - minX;
    double height = maxY - minY;
    
    minX -= width * 0.05;
    maxX += width * 0.05;
    minY -= height * 0.05;
    maxY += height * 0.05;
    
    fprintf(gp, "set xrange [%f:%f]\n", minX-500, maxX+500);
    fprintf(gp, "set yrange [%f:%f]\n", minY-500, maxY+500);
    fprintf(gp, "set size ratio %f\n", height / width);
    fprintf(gp, "unset key\n");
//    printf("X range = [%f:%f]\n",minX-500, maxX+500);
//    printf("Y range = [%f:%f]\n",minY-500, maxY+500);
}

void Gnuplot::set3Dmode() {
//    fprintf(gp, "set terminal x11\n");
    fprintf(gp, "set mouse\n");
    fprintf(gp, "set ticslevel 0\n");
    fprintf(gp, "set view equal xyz\n");
    fprintf(gp, "unset key\n");
}

void Gnuplot::end3Dmode() {
    fprintf(gp, "reset\n");
//    fprintf(gp, "set terminal aqua\n");
}

void Gnuplot::setTitle(const string title) {
    fprintf(gp, "set title \"%s\"\n", title.c_str());
}

void Gnuplot::setOutputEps(const string outputFile) {
    fprintf(gp, "set terminal postscript enhanced color\n");
    fprintf(gp, "set output \"%s\"\n", outputFile.c_str());
}

void Gnuplot::setSize(const int minx, const int miny, const int maxx, const int maxy) {
    double width = maxx - minx;
    double height = maxy - miny;
    fprintf(gp, "set xrange [%d:%d]\n", minx, maxx);
    fprintf(gp, "set yrange [%d:%d]\n", miny, maxy);
    fprintf(gp, "set size ratio %f\n", height / width);
    fprintf(gp, "unset key\n");
}

void Gnuplot::draw(const vector<Point> &vp) {
    resize({vp}, {});
//    setSize(-14000, -18000, 21000, 21000);
//    Gnuplot::setOutputEps("/Users/hikaru/research/論文/SMARTCOMP/LRSdata_14.eps");
    
//    fprintf(gp, "plot '-' with p pt 7 ps 0.3 lc 'red'\n");
    fprintf(gp, "plot '-' with p pt 6 ps 0.5 lc 'red', '-' w p pt 5 ps 2 lc 'black'\n");
    multiple_plot(vp);
    vector<Point> v;
    v.push_back(Point(0, 0));
    multiple_plot(v);
}

void Gnuplot::drawPolygon(const vector<Point> &vp) {
    resize({vp}, {});
    
    fprintf(gp, "plot '-' with line lw 3 lc 'red'\n");
    multiple_plot(vp);
}

void Gnuplot::draw(const vector<Point> &vp, const vector<LineSegment> &vl) {
//    resize({vp}, {vl});
    setSize(-14000, -18000, 21000, 21000);
    Gnuplot::setOutputEps("/Users/hikaru/Desktop/LRSdata_15_LS.eps");
    
//    fprintf(gp, "plot '-' with p ps 0.2 lc 'red' , '-' w lp pt 7 ps 0.6 lw 2 lc 'blue' \n");
    fprintf(gp, "plot '-' with p pt 7 ps 0.8 lc 'red' , '-' w lp pt 0 ps 2 lw 10 lc 'blue', '-' w p pt 5 ps 2 lc 'black' \n");
    multiple_plot(vp);
    multiple_plot(vl);
    vector<Point> v;
    v.push_back(Point(0, 0));
    multiple_plot(v);
}

void Gnuplot::draw(const vector<Point3D> &vp) {
//    resize({vp}, {});
    set3Dmode();
    
    fprintf(gp, "splot '-' with p pt 7 ps 0.3 lc 'red'\n");
    multiple_plot(vp);
    fprintf(gp, "set mouse\n");
//    end3Dmode();
}

void Gnuplot::draw(const vector<Point3D> &vp1, const vector<Point3D> &vp2) {
//    resize({vp}, {vl});
    set3Dmode();
    fprintf(gp, "splot '-' with p ps 0.2 lc 'red' , '-' w p ps 0.2 lc 'blue' \n");
    multiple_plot(vp1);
    multiple_plot(vp2);
//    end3Dmode();
}

void Gnuplot::draw(const vector<Point> &vp, const vector<Point> &vp2, const vector<LineSegment> &vl) {
    resize({vp, vp2}, {vl});
    
    fprintf(gp, "plot '-' with p pt 7 ps 0.2 lc 'dark-violet' , '-' w p pt 7 ps 1 lc 'red', '-' w lp pt 7 ps 0.6 lw 2 lc 'blue'\n");
    multiple_plot(vp);
    multiple_plot(vp2);
    multiple_plot(vl);
}

void Gnuplot::draw(const vector<Point> &vp1, const vector<Point> &vp2) {
//    resize({vp1, vp2}, {});
    setSize(-14000, -18000, 21000, 21000);
    Gnuplot::setOutputEps("/Users/hikaru/Desktop/LRSdata_15_EP.eps");
    
//    fprintf(gp, "plot '-' w p pt 7 ps 0.5 lc 'red', '-' w p pt 7 ps 0.5 lc 'blue'\n");
//    fprintf(gp, "plot '-' w p pt 7 ps 0.5 lc 'blue', '-' w p pt 13 ps 2 lc 'red'\n");
    fprintf(gp, "plot '-' w p pt 7 ps 0.7 lc 'orange-red', '-' w p pt 7 ps 1.6 lc 'blue', '-' w p pt 5 ps 2 lc 'black'\n");
    multiple_plot(vp1);
    multiple_plot(vp2);
    vector<Point> v;
    v.push_back(Point(0, 0));
    multiple_plot(v);
}

void Gnuplot::drawPolygon(const vector<Point> &vp1, const vector<Point> &vp2) {
    resize({vp1, vp2}, {});
    
    fprintf(gp, "plot '-' with line lw 3 lc 'red', '-' with line lw 3 lc 'blue'\n");
    multiple_plot(vp1);
    multiple_plot(vp2);
}

void Gnuplot::draw(const vector<LineSegment> &vl) {
    resize({}, {vl});
    
    fprintf(gp, "plot '-' with lp pt 7 ps 0.6 lw 2 \n");
    multiple_plot(vl);
}

void Gnuplot::draw(const vector<LineSegment> &vl1, const vector<LineSegment> &vl2) {
    resize({}, {vl1, vl2});
    
    fprintf(gp, "plot '-' with linespoints, '-' with linespoints\n");
    multiple_plot(vl1);
    multiple_plot(vl2);
}

void Gnuplot::draw(const vector<Point> &vp1, const vector<Point> &vp2, const vector<LineSegment> &vl1, const vector<LineSegment> &vl2) {
    resize({vp1, vp2}, {vl1, vl2});
    
    fprintf(gp, "plot '-' w d, '-' w d, '-' with lp pt 7 ps 0.4 lc rgb 'dark-violet', '-' with lp pt 7 ps 0.4 lc rgb 'sea-green'\n");
    multiple_plot(vp1);
    multiple_plot(vp2);
    multiple_plot(vl1);
    multiple_plot(vl2);
}

void Gnuplot::draw(const vector<vector<Point> > &vps) {
    Gnuplot::setOutputEps("/Users/hikaru/Desktop/LRSdata_15_GP.eps");
    string command = "plot '-' w p pt 7 ps 0.8";
//    resize(vps, {});
    setSize(-14000, -18000, 21000, 21000);
    for (int i=0; i<vps.size()-1; ++i) {
        command += ", '-' w p pt 7 ps 0.8";
    }
    command += ", '-' w p pt 5 ps 2 lc 'black'";
    command += "\n";
//    cout << command << endl;
//    cout << vps.size() << endl;
    vector<Point> v;
    v.push_back(Point(0, 0));
    fprintf(gp, "%s", command.c_str());
    for (vector<Point> vp : vps) {
        multiple_plot(vp);
    }
    multiple_plot(v);
}

void Gnuplot::draw(const vector<vector<Point>> &vps, const vector<vector<LineSegment>> &vls) {
    string command = "plot '-' w p ps 0.5 lc 'red'";
    resize(vps, vls);
    for (int i=0; i<vps.size()-1; ++i) {
        command += ", '-' w p ps 0.5 lc 'red'";
    }
    for (int i=0; i<vls.size(); ++i) {
        command += ", '-' w lp pt 1 ps 0.7 lw 2 lc rgb 'blue'";
    }
    command += "\n";
    fprintf(gp, "%s", command.c_str());
    for (vector<Point> vp : vps) {
        multiple_plot(vp);
    }
    for (vector<LineSegment> ls: vls) {
        multiple_plot(ls);
    }
}

void Gnuplot::draw(const string fileName) {
    fprintf(gp, "plot \"%s\" w p pt 7 ps 0.2 lc rgb 'red' \n", fileName.c_str());
    fflush(gp);
}

void Gnuplot::end() {
    fprintf(gp,"exit\n");
    fflush(gp);
    pclose(gp);
}

void Gnuplot::multiple_plot(const vector<Point> &vp) {
    for (Point p: vp) {
        fprintf(gp, "%f %f\n", p.x, p.y);
    }
    fprintf(gp, "e\n");
}

void Gnuplot::multiple_plot(const vector<LineSegment> &vl) {
    for (LineSegment l: vl) {
        fprintf(gp, "%f %f\n", l.p.x, l.p.y);
        fprintf(gp, "%f %f\n", l.q.x, l.q.y);
        fprintf(gp, "\n");
    }
    fprintf(gp, "e\n");
}

void Gnuplot::multiple_plot(const vector<Point3D> &vp) {
    for (Point3D p: vp) {
        fprintf(gp, "%f %f %f\n", p.x, p.y, p.z);
    }
    fprintf(gp, "e\n");
    fflush(gp);
}

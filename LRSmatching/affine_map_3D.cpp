//
//  affine_map_3D.cpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/12/11.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#include "affine_map_3D.hpp"

AffineMap3D::AffineMap3D(double theta, double tx, double ty, double tz) {
    matrix = vector<vector<double>>(4, vector<double>(4, 0));
    matrix[0][0] = cos(theta);
    matrix[0][1] = -sin(theta);
    matrix[0][3] = tx;
    matrix[1][0] = sin(theta);
    matrix[1][1] = cos(theta);
    matrix[1][3] = ty;
    matrix[2][2] = 1;
    matrix[2][3] = tz;
    matrix[3][3] = 1;
}

AffineMap3D::AffineMap3D(double cosine, double sine, double tx, double ty, double tz) {
    matrix = vector<vector<double>>(4, vector<double>(4, 0));
    matrix[0][0] = cosine;
    matrix[0][1] = -sine;
    matrix[0][3] = tx;
    matrix[1][0] = sine;
    matrix[1][1] = cosine;
    matrix[1][3] = ty;
    matrix[2][2] = 1;
    matrix[2][3] = tz;
    matrix[3][3] = 1;
}

void AffineMap3D::timeOfAffineMaps(AffineMap3D affine) {
    vector<vector<double>> matrix_tmp(4, vector<double>(4, 0));
    for (int i=0; i<4; ++i) {
        for (int j=0; j<4; ++j) {
            for (int k=0; k<4; ++k) {
                matrix_tmp[i][j] += affine.matrix[i][k] * matrix[k][j];
            }
        }
    }
    matrix = matrix_tmp;
}

Point3D AffineMap3D::transform(const Point3D &p) const {
    double px = p.x * matrix[0][0] + p.y * matrix[0][1] + p.z * matrix[0][2] + 1 * matrix[0][3];
    double py = p.x * matrix[1][0] + p.y * matrix[1][1] + p.z * matrix[1][2] + 1 * matrix[1][3];
    double pz = p.x * matrix[2][0] + p.y * matrix[2][1] + p.z * matrix[2][2] + 1 * matrix[2][3];
    return Point3D(px, py, pz);
}

double AffineMap3D::angle() const {
    return atan2(matrix[1][0], matrix[1][1]);
}

double AffineMap3D::trans_x() const {
    return matrix[0][3];
}

double AffineMap3D::trans_y() const {
    return matrix[1][3];
}

double AffineMap3D::trans_z() const {
    return matrix[2][3];
}

//to_string
ostream& operator << (ostream &os, const AffineMap3D& am) {
    os << "<AffineMap: angle = " << RAD2DEG(am.angle()) << "[deg], trans_x = " << am.trans_x() << ", trans_y = " << am.trans_y() << ", trans_z = " << am.trans_z() << ">" ;
    return os;
}



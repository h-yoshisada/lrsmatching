//
//  affine_map.cpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/06/13.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#include "affine_map.hpp"

AffineMap::AffineMap(double theta, double tx, double ty) {
    matrix = vector< vector< double >>(3, vector<double>(3, 0) );
    matrix[0][0] = cos(theta);
    matrix[0][1] = -sin(theta);
    matrix[0][2] = tx;
    matrix[1][0] = sin(theta);
    matrix[1][1] = cos(theta);
    matrix[1][2] = ty;
    matrix[2][2] = 1;
}

AffineMap::AffineMap(double cosine, double sine, double tx, double ty) {
    matrix = vector< vector< double >>(3, vector<double>(3, 0) );
    matrix[0][0] = cosine;
    matrix[0][1] = -sine;
    matrix[0][2] = tx;
    matrix[1][0] = sine;
    matrix[1][1] = cosine;
    matrix[1][2] = ty;
    matrix[2][2] = 1;
}

void AffineMap::inverse(){
    vector<vector<double>> matrix_tmp(3, vector<double>(3, 0));
    matrix_tmp[0][0] = matrix[0][0];
    matrix_tmp[0][1] = -1 * matrix[0][1];
    matrix_tmp[0][2] = -1 * matrix[0][2] * matrix[0][0] + matrix[1][2] * matrix[0][1];
    matrix_tmp[1][0] = -1 * matrix[1][0];
    matrix_tmp[1][1] = matrix[1][1];
    matrix_tmp[1][2] = matrix[0][2] * matrix[1][0] - matrix[1][2] * matrix[0][0];
    matrix_tmp[2][2] = matrix[2][2];
    matrix = matrix_tmp;
}

void AffineMap::timeOfAffineMaps(AffineMap affine) {
    vector<vector<double>> matrix_tmp(3, vector<double>(3, 0));
    for (int i=0; i<3; ++i) {
        for (int j=0; j<3; ++j) {
            for (int k=0; k<3; ++k) {
                matrix_tmp[i][j] += affine.matrix[i][k] * matrix[k][j];
            }
        }
    }
    matrix = matrix_tmp;
}

LineSegment AffineMap::transform(const LineSegment &l) const {
    Point p1 = transform(l.p);
    Point p2 = transform(l.q);
    return LineSegment(p1, p2);
}

Point AffineMap::transform(const Point &p) const {
    double px = p.x * matrix[0][0] + p.y * matrix[0][1] + 1 * matrix[0][2];
    double py = p.x * matrix[1][0] + p.y * matrix[1][1] + 1 * matrix[1][2];
    return Point(px, py);
}

vector<LineSegment> AffineMap::transform(const vector<LineSegment> ls) const {
    vector<LineSegment> ret;
    for (LineSegment l: ls) {
        ret.push_back(this->transform(l));
    }
    return ret;
}

double AffineMap::angle() const {
    return atan2(matrix[1][0], matrix[1][1]);
}

double AffineMap::trans_x() const {
    return matrix[0][2];
}

double AffineMap::trans_y() const {
    return matrix[1][2];
}

// to_string
ostream& operator << (std::ostream &os, const AffineMap& am) {
    os << "<AffineMap: angle = " << RAD2DEG(am.angle()) << "[deg], trans_x = " << am.matrix[0][2] << ", trans_y = " << am.matrix[1][2] << ">";
    return os;
}

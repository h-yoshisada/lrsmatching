//
//  affine_map.hpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/06/13.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#ifndef affine_map_hpp
#define affine_map_hpp

#include <cmath>
#include <vector>

#include "line_segment.hpp"
#include "point.hpp"

using namespace std;

class AffineMap {
public:
    vector< vector< double >> matrix;
    
    AffineMap(double theta, double tx, double ty);
    AffineMap(double cosine, double sine, double tx, double ty);
    
    void inverse();
    void timeOfAffineMaps(AffineMap affine);
    
    LineSegment transform(const LineSegment &l) const;
    Point transform(const Point &p) const;
    vector<LineSegment> transform(const vector<LineSegment> ls) const;
    
    // 回転角を返すメソッド
    double angle() const;
    // 平行移動量を返すメソッド
    double trans_x() const;
    double trans_y() const;
    
    // to_string
    friend ostream& operator << (std::ostream &os, const AffineMap& am);
};

#endif /* affine_map_hpp */

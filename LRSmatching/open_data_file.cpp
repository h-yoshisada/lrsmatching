//
//  open_data_file.cpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/06/13.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#include "open_data_file.hpp"

vector<double> OpenDataFile::readCSV(string filename){
    vector<double> datas;
    vector< vector<double> > tmpDatas(1081, vector<double>(0, 0));
    
    string dataLine;
    string field;
    ifstream ifs(filename);
    
    if (ifs.fail()) {
        cerr << "Error: Can't find data file." << endl;
        exit(1);
    }

    int count = 0;
    auto itr = tmpDatas.begin();
    while(getline(ifs,dataLine) && count < NUM_DATALINE){
        itr = tmpDatas.begin();
        istringstream stream(dataLine);
        while(getline(stream, field, ',')){
            if(stoi(field)==0) continue;
            itr->push_back(stoi(field));
            ++itr;
        }
        count ++;
    }
    for (vector<double> v: tmpDatas) {
        sort(v.begin(), v.end());
//        double data = (v[(NUM_DATALINE/2) - 1] + v[NUM_DATALINE/2]) / 2;
        double data = v[NUM_DATALINE/2];
        datas.push_back(data);
    }
    
    cout << "data lines of " << filename << ": " << count << endl;
    return datas;
}

vector<Point> OpenDataFile::coordinateTransformation(vector<double> &data){
    vector<Point> points;
    
    double param = -135;
    for (unsigned int it = 0; it < data.size(); ++it) {
//        if (data[it] > 25000) {
//            param += 0.25;
//            continue;
//        }
        double x = data[it] * cos(DEG2RAD(param));
        double y = data[it] * sin(DEG2RAD(param));
        points.push_back(Point(x, y));
        
        param += 0.25;
        
        if (data[it] < 500) {
            it += 20;
            param += 5;
        } else if(data[it] < 1000) {
            it += 10;
            param += 2.5;
        }
    }
    return points;
}

vector<Point3D> OpenDataFile::coordinateTransformation(vector<double> &data, double angle, double height) {
    vector<Point3D> points;
    
    double psy = DEG2RAD(angle);
    double theta = -135;
    for (int it=0; it<data.size(); ++it) {
        double x = data[it] * cos(DEG2RAD(theta)) * cos(psy);
        double y = data[it] * sin(DEG2RAD(theta));
        double z = height + (data[it] * cos(DEG2RAD(theta)) * sin(psy));
        
        if (z > 0) {
            points.push_back(Point3D(x, y, z));
        }
        
        theta += 0.25;
        if (data[it] < 500) {
            it += 20;
            theta += 5;
        } else if (data[it] < 1000) {
            it += 10;
            theta += 2.5;
        }
    }
    
    return points;
}

//
//  graph.cpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/12/28.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#include "graph.hpp"

Graph::Graph(const vector<int> &lrsNum) {
    int number = (int) lrsNum.size();
    copy(lrsNum.begin(), lrsNum.end(), back_inserter(lrsNumberList));
    adjacencyMatrix = vector<vector<int>>(number, vector<int>(number, 0));
    visitedMatrix = vector<vector<int>>(number, vector<int>(number, 0));
    resultMatrix = vector<vector<AffineMap>>(number, vector<AffineMap>(number, AffineMap(0, 0, 0)));
    weightMatrix = vector<vector<double>>(number, vector<double>(number, 0));
}

void Graph::makeEdges(const vector<pair<int, int> > &vLrsPairs) {
    for (pair<int, int> p : vLrsPairs) {
        int lrs1 = p.first, lrs2 = p.second;
        auto index1 = distance(lrsNumberList.begin(), find(lrsNumberList.begin(), lrsNumberList.end(), lrs1));
        auto index2 = distance(lrsNumberList.begin(), find(lrsNumberList.begin(), lrsNumberList.end(), lrs2));
        adjacencyMatrix[index1][index2] = 1;
        adjacencyMatrix[index2][index1] = 1;
    }
}

void Graph::makeEdges() {
    cout << "Input the node number pair with space (ex. 3 5)." << endl;
    cout << "If you want to finish, input -1." << endl;
    for (;;) {
        cout << "Lrs pair > ";
        int lrs1, lrs2;
        cin >> lrs1;
        if (lrs1==-1) break;
        else cin >> lrs2;
        if (find(lrsNumberList.begin(), lrsNumberList.end(), lrs1)!=lrsNumberList.end() && find(lrsNumberList.begin(), lrsNumberList.end(), lrs2)!=lrsNumberList.end()) {
            auto index1 = distance(lrsNumberList.begin(), find(lrsNumberList.begin(), lrsNumberList.end(), lrs1));
            auto index2 = distance(lrsNumberList.begin(), find(lrsNumberList.begin(), lrsNumberList.end(), lrs2));
            adjacencyMatrix[index1][index2] = 1;
            adjacencyMatrix[index2][index1] = 1;
        } else {
            cout << "Data not exist. Input again." << endl;
            continue;
        }
    }
    
    for (vector<int> v : adjacencyMatrix) {
        for (int i : v) {
            cout << i << " " ;
        }
        cout << endl;
    }
}

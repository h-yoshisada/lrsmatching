//
//  point3D.hpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/11/30.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#ifndef point3D_h
#define point3D_h

#include <cmath>
#include <iostream>

using namespace std;

class Point3D {
public:
    double x, y, z;
    
public:
    Point3D() {
        this->x = this->y = this->z = 0;
    }
    
    Point3D(double x, double y, double z) {
        this->x = x;
        this->y = y;
        this->z = z;
    }
    
    Point3D clone() const {
        return Point3D(x, y, z);
    }
    
    //3Dでの２点間の距離
    double distance3D(Point3D p) const {
        return sqrt(pow(p.x - x, 2) + pow(p.y - y, 2) + pow(p.z - z, 2));
    }
    
    double distance2D(Point3D p) const {
        return sqrt(pow(p.x - x, 2) + pow(p.y - y, 2));
    }
    
    // to_string
    friend ostream& operator << (ostream &os, const Point3D& p) {
        os << "(" << p.x << ", " << p.y << ", " << p.z << ")";
        return os;
    }
    
    // 比較演算子
    bool operator == (const Point3D& p) const {
        return compare(p) == 0;
    }
    
    bool operator > (const Point3D& p) const {
        return compare(p) == +1;
    }
    
    bool operator < (const Point3D& p) const {
        return compare(p) == -1;
    }
    
    bool operator != (const Point3D& p) const {
        return compare(p) != 0;
    }
    
    bool operator >= (const Point3D& p) const {
        return compare(p) >= 0;
    }
    
    bool operator <= (const Point3D& p) const {
        return compare(p) <= 0;
    }
    
private:
    int compare (const Point3D& p) const {
        if (this->x == p.x) {
            if (this->y > p.y) return +1;
            else if (this->y < p.y) return -1;
            else
                if (this->z > p.z) return +1;
                else if (this->z < p.z) return -1;
                else return 0;
        } else {
            if (this->x > p.x) return +1;
            else return -1;
        }
    }
    
    
};


#endif /* point3D_h */

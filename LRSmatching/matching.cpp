//
//  matching.cpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/06/13.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#include <algorithm>
#include <unistd.h>
#include <tuple>

#include "matching.hpp"

#define BIN_NUM (2 * 6)
#define BIN_SIZE (M_PI / BIN_NUM)

vector<vector<LineSegment>> Matching::clustering(vector<LineSegment> lines) {
//    線分の集合をBIN_NUM個にクラスタリング
    map<int, vector<int>> mp1, mp2;
    for (int i=0; i<BIN_NUM; ++i) {
        mp1[i].clear();
        mp2[i].clear();
    }
    
    for (unsigned int i=0; i<lines.size(); ++i) {
        int idx1 = ((int) (lines[i].angle() / BIN_SIZE)) % BIN_NUM;
        int idx2 = ((int) (lines[i].angle() / BIN_SIZE + 0.5)) % BIN_NUM;
        mp1[idx1].push_back(i);
        mp2[idx2].push_back(i);
    }
    
    vector<vector<LineSegment>> ret;    {
        vector< map<int, vector<int> >> bins{mp1, mp2};
        
        // 投票の最大値を計算する
        int max_vote = -1;
        for (int b=0;b<bins.size();++b) {
            for (int i=0;i<BIN_NUM/2;++i) {
                int sum_vote = (int) (bins[b][i].size() + bins[b][i+BIN_NUM/2].size());
                max_vote = max(max_vote, sum_vote);
            }
        }
        
        // 投票が最大の線分群を返す
        for (int b=0;b<bins.size();++b) {
            bool isMatch = false;
            for (int i=0;i<BIN_NUM/2;++i) {
                int sum_vote = (int) (bins[b][i].size() + bins[b][i+BIN_NUM/2].size());
                if (sum_vote == max_vote) {
                    ret.push_back(vector< LineSegment >());
                    ret.push_back(vector< LineSegment >());
                    for (int j=0;j<bins[b][i].size();j++) {
                        ret[0].push_back(lines[bins[b][i][j]]);
                    }
                    for (int j=0;j<bins[b][i+BIN_NUM/2].size();j++) {
                        ret[1].push_back(lines[bins[b][i+BIN_NUM/2][j]]);
                    }
                    isMatch = true;
                    break;
                }
            }
            if (isMatch) break;
        }
    }
    
    return ret;
}

void Matching::matching(const vector<LineSegment> lines1, const vector<LineSegment> lines2, vector<Point> keyPoints1, vector<Point> keyPoints2, int lrs_1, int lrs_2, vector<Result> &results) {
    results.clear();
    int count = 0;
    for (LineSegment line1: lines1) {
        count += 1;
        for (LineSegment line2: lines2) {
            double tmpAngle = line1.angle() - line2.angle();
            vector<double> angles {tmpAngle, tmpAngle + M_PI};
            for (double angle: angles) {
                Result thisPairsMaxResult1 = Result{0, 0, 0, 0, 0, 0, 0, 0, LINESEGMENT_MATCHING};
                double org_trans_x_1, org_trans_y_1;
                {
                    AffineMap affineMap(angle, 0, 0);
                    LineSegment ls = affineMap.transform(line2);
                    org_trans_x_1 = line1.center().x - ls.center().x;
                    org_trans_y_1 = line1.center().y - ls.center().y;
                }
                for (double trans_d = -10000; trans_d<= +10000; trans_d+=100) {
                    double trans_x = org_trans_x_1 + trans_d * cos(line1.angle());
                    double trans_y = org_trans_y_1 + trans_d * sin(line1.angle());
                    AffineMap transAffine = AffineMap(angle, trans_x, trans_y);
                    {
                        vector<LineSegment> transLineSegments = transAffine.transform(lines2);
                        vector<Point> transKeyPoints;
                        for (Point p: keyPoints2) {
                            transKeyPoints.push_back(transAffine.transform(p));
                        }
                        double similarity = 0, lineSegmentSimilarity = 0, keyPointSimilarity = 0;
                        tie (similarity, lineSegmentSimilarity, keyPointSimilarity) = calc_sim_tuple(lines1, transLineSegments, keyPoints1, transKeyPoints);
                        if (similarity >= thisPairsMaxResult1.sim) {
                            //                            if (checkConsistency(tmpLines1, tmpLines2, transAffine)==true) {
                            thisPairsMaxResult1 = Result{similarity, angle, trans_x, trans_y, 0, 0, lineSegmentSimilarity, keyPointSimilarity, LINESEGMENT_MATCHING};
                            //                            }
                        }
                    }
                }
                results.push_back(thisPairsMaxResult1);
            }
        }
        cout << "lineSegment : step " << count << " ended." << endl;
    }
    sort(results.begin(), results.end());
    if (results.size() > 5) {
        results.erase(results.begin() + 5, results.end());
    }
    //    for (Result rst: results) {
    //        cout << "(sim, angle, trans_x, trans_y, keypoint_x, keypoint_y, lsSim, kpSim, mode) = (" << rst.sim << ", " << rst.trans_x << ", " << rst.trans_y << ", " << rst.keyPoint_x << ", " << rst.keyPoint_y << ", " << rst.lineSegmentSililarity << ", " << rst.keyPointSimilarity << ", " << rst.matchingMethod << ")" << endl;
    //    }
}

void Matching::matchingLineSegmentsThread(const vector<LineSegment> lines1, const vector<LineSegment> lines2, vector<Point> keyPoints1, vector<Point> keyPoints2, int lrs_1, int lrs_2, int startIndex, int endIndex, vector<Result> &results) {
    for (int i=startIndex; i<endIndex; ++i) {
        LineSegment line1 = lines1[i];
        for (int j=0; j<lines2.size(); ++j) {
            Result thisPairsMaxResult1 = Result{0, 0, 0, 0, 0, 0, 0, 0, LINESEGMENT_MATCHING};
            LineSegment line2 = lines2[j];
            double tmpAngle = line1.angle() - line2.angle();
            if (abs(RAD2DEG(tmpAngle))>20) {
                continue;
            }
            vector<double> angles {tmpAngle, tmpAngle + M_PI};
            for (double angle: angles) {
                double org_trans_x_1, org_trans_y_1;
                {
                    AffineMap affineMap(angle, 0, 0);
                    LineSegment ls = affineMap.transform(line2);
                    org_trans_x_1 = line1.center().x - ls.center().x;
                    org_trans_y_1 = line1.center().y - ls.center().y;
                }
                for (double trans_d = -5000; trans_d<= +5000; trans_d+=100) {
                    double trans_x = org_trans_x_1 + trans_d * cos(line1.angle());
                    double trans_y = org_trans_y_1 + trans_d * sin(line1.angle());
                    AffineMap transAffine = AffineMap(angle, trans_x, trans_y);
                    {
                        vector<LineSegment> transLineSegments = transAffine.transform(lines2);
                        vector<Point> transKeyPoints;
                        for (Point p: keyPoints2) {
                            transKeyPoints.push_back(transAffine.transform(p));
                        }
                        double similarity = 0, lineSegmentSimilarity = 0, keyPointSimilarity = 0;
                        double similarity2nd = 0, lineSegmentSimilarity2nd = 0, keyPointSimilarity2nd = 0;
                        tie (similarity, lineSegmentSimilarity, keyPointSimilarity) = calc_sim_tuple(lines1, transLineSegments, keyPoints1, transKeyPoints);
                        tie (similarity2nd, lineSegmentSimilarity2nd, keyPointSimilarity2nd) = calc_sim_tuple(transLineSegments, lines1, transKeyPoints, keyPoints1);
                        if (similarity2nd >= similarity) {
                            similarity = similarity2nd;
                            lineSegmentSimilarity = lineSegmentSimilarity2nd;
                            keyPointSimilarity = keyPointSimilarity2nd;
                        }
                        if (similarity > thisPairsMaxResult1.sim) {
                            if (checkConsistency(lines1, transLineSegments, transAffine)==false) continue;
                            thisPairsMaxResult1 = Result{similarity, angle, trans_x, trans_y, 0, 0, lineSegmentSimilarity, keyPointSimilarity, LINESEGMENT_MATCHING};
                        }
                    }
                }
            }
            results.push_back(thisPairsMaxResult1);
        }
        cout << "lineSegment : step " << i+1 << " ended." << endl;
    }
    //    for (Result rst: results) {
    //        cout << "(sim, angle, trans_x, trans_y, keypoint_x, keypoint_y, lsSim, kpSim, mode) = (" << rst.sim << ", " << rst.trans_x << ", " << rst.trans_y << ", " << rst.keyPoint_x << ", " << rst.keyPoint_y << ", " << rst.lineSegmentSililarity << ", " << rst.keyPointSimilarity << ", " << rst.matchingMethod << ")" << endl;
    //    }
}

void Matching::matchingKeyPoints(const vector<LineSegment> lines1, const vector<LineSegment> lines2, vector<Point> keyPoints1, vector<Point> keyPoints2, int lrs_1, int lrs_2, vector<Result> &results) {
    
    results.clear();
    for (int i=0; i<keyPoints1.size(); ++i) {
        Result tmpResult = Result{0, 0, 0, 0, 0, 0, 0, 0, KEYPOINT_MATCHING};
        for (int j=0; j<keyPoints2.size(); ++j) {
            double trans_x = keyPoints1[i].x - keyPoints2[j].x;
            double trans_y = keyPoints1[i].y - keyPoints2[j].y;
            AffineMap moveAffine = AffineMap(0, trans_x - keyPoints1[i].x, trans_y - keyPoints1[i].y);
            for (double deg=0; deg<360; deg+=0.5) {
                AffineMap rotateAffine = AffineMap(DEG2RAD(deg), keyPoints1[i].x, keyPoints1[i].y);
                vector<LineSegment> transLineSegments = rotateAffine.transform(moveAffine.transform(lines2));
                vector<Point> transKeyPoints;
                for (Point p: keyPoints2) {
                    transKeyPoints.push_back(rotateAffine.transform(moveAffine.transform(p)));
                }
                double similarity = 0, lineSegmentSimilarity = 0, keyPointSimilarity = 0;
                double similarity2nd = 0, lineSegmentSimilarity2nd = 0, keyPointSimilarity2nd = 0;
                tie (similarity, lineSegmentSimilarity, keyPointSimilarity) = calc_sim_tuple(lines1, transLineSegments, keyPoints1, transKeyPoints);
                tie (similarity2nd, lineSegmentSimilarity2nd, keyPointSimilarity2nd) = calc_sim_tuple(transLineSegments, lines1, transKeyPoints, keyPoints1);
                if (similarity2nd >= similarity) {
                    similarity = similarity2nd;
                    lineSegmentSimilarity = lineSegmentSimilarity2nd;
                    keyPointSimilarity = keyPointSimilarity2nd;
                }
                if (similarity >= tmpResult.sim) {
                    tmpResult = Result{similarity, DEG2RAD(deg), trans_x, trans_y, keyPoints1[i].x, keyPoints1[i].y, lineSegmentSimilarity, keyPointSimilarity, KEYPOINT_MATCHING};
                }
            }
        }
        results.push_back(tmpResult);
        cout << "keyPoint : step " << i+1 << " ended." << endl;
    }
    sort(results.begin(), results.end());
    if (results.size() > 5) {
        results.erase(results.begin() + 5, results.end());
    }
//    for (Result rst: results) {
//        cout << "(sim, angle, trans_x, trans_y, keypoint_x, keypoint_y, lsSim, kpSim, mode) = (" << rst.sim << ", " << rst.trans_x << ", " << rst.trans_y << ", " << rst.keyPoint_x << ", " << rst.keyPoint_y << ", " << rst.lineSegmentSililarity << ", " << rst.keyPointSimilarity << ", " << rst.matchingMethod << ")" << endl;
//    }
}

void Matching::matchingKeyPointsThread(const vector<LineSegment> lines1, const vector<LineSegment> lines2, vector<Point> keyPoints1, vector<Point> keyPoints2, int lrs_1, int lrs_2, int startIndex, int endIndex, vector<Result> &results) {
    double trans_x, trans_y;
    for (int i=startIndex; i<endIndex; ++i) {
        Result tmpResult = Result{0, 0, 0, 0, 0, 0, 0, 0, KEYPOINT_MATCHING};
        for (int j=0; j<keyPoints2.size(); ++j) {
            trans_x = keyPoints1[i].x - keyPoints2[j].x;
            trans_y = keyPoints1[i].y - keyPoints2[j].y;
            AffineMap moveAffine = AffineMap(0, trans_x - keyPoints1[i].x, trans_y - keyPoints1[i].y);
            for (double deg=0.0; deg<360; deg+=1.0) {
                AffineMap rotateAffine = AffineMap(DEG2RAD(deg), keyPoints1[i].x, keyPoints1[i].y);
                AffineMap transAffine = moveAffine;
                transAffine.timeOfAffineMaps(rotateAffine);
                vector<LineSegment> transLineSegments = transAffine.transform(lines2);
                vector<Point> transKeyPoints;
                for (Point p: keyPoints2) {
                    transKeyPoints.push_back(transAffine.transform(p));
                }
                double similarity = -1, lineSegmentSimilarity = -1, keyPointSimilarity = -1;
                double similarity2nd = -1, lineSegmentSimilarity2nd = -1, keyPointSimilarity2nd = -1;
                tie (similarity, lineSegmentSimilarity, keyPointSimilarity) = calc_sim_tuple(lines1, transLineSegments, keyPoints1, transKeyPoints);
                tie (similarity2nd, lineSegmentSimilarity2nd, keyPointSimilarity2nd) = calc_sim_tuple(transLineSegments, lines1, transKeyPoints, keyPoints1);
                if (similarity2nd >= similarity) {
                    similarity = similarity2nd;
                    lineSegmentSimilarity = lineSegmentSimilarity2nd;
                    keyPointSimilarity = keyPointSimilarity2nd;
                }
                if (similarity > tmpResult.sim) {
//                    if (checkConsistency(lines1, transLineSegments, transAffine)==false) continue;
                    tmpResult = Result{similarity, DEG2RAD(deg), trans_x, trans_y, keyPoints1[i].x, keyPoints1[i].y, lineSegmentSimilarity, keyPointSimilarity, KEYPOINT_MATCHING};
                }
            }
        }
        results.push_back(tmpResult);
        cout << "keyPoint : step " << i+1 << " ended." << endl;
    }
    //    for (Result rst: results) {
    //        cout << "(sim, angle, trans_x, trans_y, keypoint_x, keypoint_y, lsSim, kpSim, mode) = (" << rst.sim << ", " << rst.trans_x << ", " << rst.trans_y << ", " << rst.keyPoint_x << ", " << rst.keyPoint_y << ", " << rst.lineSegmentSililarity << ", " << rst.keyPointSimilarity << ", " << rst.matchingMethod << ")" << endl;
    //    }
}

tuple<double, double, double> Matching::calc_sim_tuple(vector<LineSegment> lines1, vector<LineSegment> lines2, vector<Point> keyPoints1, vector<Point> keyPoints2) {
    double alpha = 0.4;
    double similarity = 0;
    double lineSegmentSimilarity = 0;
    double keyPointSimilarity = 0;
    int count = 0;
    LineSegment choisedLS(Point(0, 0), Point(0, 0));
    //線分の類似度
    double numOfLineSegments = fmin((double) lines1.size(), (double) lines2.size());
    
    for (LineSegment ls1: lines1) {
        double maxSimilarity = 0;
        for (LineSegment ls2: lines2) {
            if (abs(ls1.angle() - ls2.angle()) > 45) continue;
            double sim = ls1.similarity(ls2);
            if (sim > maxSimilarity) {
                maxSimilarity = sim;
                choisedLS = ls2;
            }
        }
        
        if (maxSimilarity > 0) {
            lines2.erase(remove_if(lines2.begin(), lines2.end(), [&choisedLS]
                                   (LineSegment ls)->bool {
                                       if (ls == choisedLS) return true;
                                       return false;
                                   }), lines2.end());
            count += 1;
        }
        lineSegmentSimilarity += maxSimilarity;
    }
    lineSegmentSimilarity  = lineSegmentSimilarity / (2 * numOfLineSegments);
    vector<double> keyPointMatchResult = KeyPoint::evaluationSimilarity(keyPoints1, keyPoints2);
    int numOfKeyPoints = (int) min(keyPoints1.size(), keyPoints2.size());
    keyPointSimilarity = (double) keyPointMatchResult.size() / numOfKeyPoints;
    similarity = alpha * lineSegmentSimilarity + (1-alpha) * keyPointSimilarity;
//    similarity = lineSegmentSimilarity + keyPointSimilarity;
    
    return forward_as_tuple(similarity, lineSegmentSimilarity, keyPointSimilarity);
}

// 変換行列のつじつまをチェック
// LRS-1の測位域にLRS-2の線分(壁)が存在したらfalse(不正)を返す
// 整合性がとれていればtrueを返す
bool Matching::checkConsistency(const vector< LineSegment > lines1, const vector< LineSegment > tmpLines2, const AffineMap affineMap) {
    vector< LineSegment > lines2 = affineMap.transform(tmpLines2);
    
    if (checkConsistency(lines1, lines2, Point(0, 0)) == false) return false;
    if (checkConsistency(lines2, lines1, affineMap.transform(Point(0, 0))) == false) return false;
    
    return true;
}

//パーティション問題（整合性取れているのに弾かれる）
bool Matching::checkConsistency(const vector< LineSegment > lines1, const vector< LineSegment > lines2, const Point lrs1Position) {
    const int SPLIT_NUM = 3;
    for (int k=0; k<lines2.size(); ++k) {
        LineSegment line2 = lines2[k];
        //        // 測定誤差があるので少し短くする
        line2 = line2.makeSmaller(800);
        for (int l=0; l<lines1.size(); ++l) {
            LineSegment line1 = lines1[l];
            // line1を(SPLIT_NUM+1)等分する (両端含む)
            for (int i=0;i<=SPLIT_NUM;i++) {
                int j = SPLIT_NUM - i;
                Point wallPoint = line1.internallyDividingPoint(i, j);
                double dx = wallPoint.x - lrs1Position.x;
                double dy = wallPoint.y - lrs1Position.y;
                LineSegment LRS_1_scanLine = LineSegment(lrs1Position, Point(lrs1Position.x + (dx * 0.7), lrs1Position.y + (dy * 0.7)));
                
                if (line2.checkIntersected(LRS_1_scanLine)==true) {
                    // LRS-2の線分とLRS-1の即位域が交差 (不正)
                    return false;
                }
            }
        }
    }
    
    return true;
}















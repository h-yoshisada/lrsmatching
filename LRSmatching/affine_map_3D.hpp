//
//  affine_map_3D.hpp
//  LRSmatching
//
//  Created by 吉貞洸 on 2017/12/11.
//  Copyright © 2017年 Hikaru Yoshisada. All rights reserved.
//

#ifndef affine_map_3D_hpp
#define affine_map_3D_hpp

#include <cmath>
#include <vector>

#include "line_segment.hpp"
#include "point3D.hpp"

using namespace std;

class AffineMap3D {
public:
    vector< vector< double >> matrix;
    
    AffineMap3D(double theta, double tx, double ty, double tz);
    AffineMap3D(double cosine, double sine, double tx, double ty, double tz);
    
    void timeOfAffineMaps(AffineMap3D affine);
    
    Point3D transform(const Point3D &p) const;
    
    // 回転角を返すメソッド
    double angle() const;
    // 平行移動量を返すメソッド
    double trans_x() const;
    double trans_y() const;
    double trans_z() const;
    
    // to_string
    friend ostream& operator << (std::ostream &os, const AffineMap3D& am);

};


#endif /* affine_map_3D_hpp */
